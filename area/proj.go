package area

import (
	"bitbucket.org/tajtiattila/harmless/db"
)

var (
	XZPlane = Projection{0, 2, 1}
)

type Projection struct {
	Xi, Yi int // 0-2, 3D coordinate to use for 2D coordinates
	Hi     int // 0-2, 3D "height" coordinate
}

func (p Projection) P(v db.Vec3) db.Vec2 { return db.Vec2{v[p.Xi], v[p.Yi]} }
func (p Projection) H(v db.Vec3) float64 { return v[p.Hi] }

func (p Projection) String() string {
	switch {
	case p.Xi == 0 && p.Yi == 1:
		return "XY"
	case p.Xi == 0 && p.Yi == 2:
		return "XZ"
	case p.Xi == 0 && p.Yi == 1:
		return "YZ"
	}
	return "invalid"
}
