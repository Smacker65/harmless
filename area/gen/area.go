package gen

import (
	"bitbucket.org/tajtiattila/harmless/area"
	"bitbucket.org/tajtiattila/harmless/db"
	"crypto"
	_ "crypto/sha512"
	"encoding/hex"
	"fmt"
	"sort"
	"strings"
)

type AreaMapper struct {
	db.Chart
	Fonts *FontMap

	Proj   area.Projection
	Radius float64
}

func NewAreaMapper(c db.Chart, p area.Projection, r float64, fm *FontMap) *AreaMapper {
	return &AreaMapper{
		Chart:  c,
		Fonts:  fm,
		Proj:   p,
		Radius: r,
	}
}

func (am *AreaMapper) Scale() float64 {
	return SphereScale(am.Radius)
}

// generate a hashable list that will stay unchanged until something is added to the map
func (am *AreaMapper) Hash(centre db.Vec3) string {
	h := crypto.SHA512.New()
	fmt.Fprintf(h, "#proj=%v scale=%v\n", am.Proj, am.Scale())
	var v []hashElem
	am.q(centre, func(sys *db.System) {
		v = append(v, hashElem{sys.Name, sys.Id, icoord(sys.P[0]), icoord(sys.P[2]), len(sys.Stations) != 0})
	})
	sort.Sort(hashSort(v))
	for _, e := range v {
		fmt.Fprintf(h, "%s\t%s\t%d\t%d\t%v\n", e.id, e.name, e.stn)
	}
	return hex.EncodeToString(h.Sum(nil))
}

func (am *AreaMapper) Map(centre db.Vec3) *area.Map {
	return am.MapIter(centre, MaxIter, nil)
}

func (am *AreaMapper) MapIter(centre db.Vec3, maxiter int, cb func(*area.Map)) *area.Map {
	scale := am.Scale()

	sectm := make(map[string]*SectorElem)
	am.q(centre, func(sys *db.System) {
		sect, _ := db.SplitLabel(sys.Name)
		if sect != "" {
			slo := strings.ToLower(sect)
			if _, ok := sectm[slo]; !ok {
				sp := &SectorElem{label: sect, scale: 1 / scale, width: am.Fonts.Sector.TextWidth(sect, 0.9) + 1}
				sectm[slo] = sp
			}
		}
	})

	//sim := NewSimulation(CircleConstraint(db.Vec2{0,0}, am.Radius*scale))
	sim := NewSimulation(1/scale, NoEffect)
	am.q(centre, func(sys *db.System) {
		e := &SysElem{System: sys, p0: am.Proj.P(sys.P.Sub(centre)), weight: 1}

		_, e.label = db.SplitLabel(sys.Name)

		e.width = sysfont(am.Fonts, sys).TextWidth(e.label, 0.8) + 1

		sim.Add(e)
	})
	for _, se := range sim.All {
		sect, _ := db.SplitLabel(se.Elem.(*SysElem).Name)
		if sect != "" {
			sp := sectm[strings.ToLower(sect)]
			sp.refs = append(sp.refs, se)
		}
	}
	for _, sp := range sectm {
		sim.Add(sp)
	}

	if cb != nil {
		sim.RunIterCallback(maxiter, func(int) {
			cb(am.mapsim(centre, sim))
		})
	} else {
		sim.RunIter(maxiter)
	}

	m := am.mapsim(centre, sim)
	m.Hash = am.Hash(centre)
	return m
}

// mapsim generates the map based on the current simulation state
func (am *AreaMapper) mapsim(centre db.Vec3, sim *Simulation) *area.Map {
	m := &area.Map{Scale: am.Scale()}
	for _, se := range sim.All {
		sp := se.S
		switch e := se.Elem.(type) {
		case *SysElem:
			sx, sy := trvec(sp)
			m.Elems = append(m.Elems, area.Sys{sx, sy, e.width, e.Id, e.label})
		case *SectorElem:
			sx, sy := trvec(sp)
			m.Labels = append(m.Labels, area.Label{sx, sy, e.label})
		}
	}
	return m
}

func (am *AreaMapper) q(c db.Vec3, f func(sys *db.System)) {
	am.Chart.Sphere(c, am.Radius, func(sys *db.System, dsys float64) { f(sys) })
}
