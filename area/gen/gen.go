package gen

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"math"
	"os"
)

type FontMap struct {
	Inhabited, Empty, Sector *Font
}

func LoadDefaultFontMap(dir string) (*FontMap, error) {
	if dir != "" && !os.IsPathSeparator(dir[len(dir)-1]) {
		dir += string(os.PathSeparator)
	}
	fontinhabited, err := LoadFont(dir + "Oswald-Regular.ttf")
	if err != nil {
		return nil, err
	}
	fontempty, err := LoadFont(dir + "Oswald-Light.ttf")
	if err != nil {
		return nil, err
	}
	fontsector, err := LoadFont(dir + "CrimsonText-Italic.ttf")
	if err != nil {
		return nil, err
	}
	return &FontMap{fontinhabited, fontempty, fontsector}, nil
}

func icoord(c float64) int32 {
	return int32(math.Floor(c*32 + 0.5))
}

type hashElem struct {
	name, id string
	x, y     int32
	stn      bool
}

type hashSort []hashElem

func (s hashSort) Len() int           { return len(s) }
func (s hashSort) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s hashSort) Less(i, j int) bool { return s[i].name < s[j].name }

type SysElem struct {
	*db.System
	p0     db.Vec2
	width  float64
	weight float64

	label string
}

func (e *SysElem) Position() db.Vec2 { return e.p0 }

func (e *SysElem) Anchor(db.Vec2) db.Vec2 { return db.Vec2{0, 0} }
func (e *SysElem) Width() float64         { return e.width }
func (e *SysElem) Weight() float64        { return e.weight }

type SectorElem struct {
	scale float64
	label string
	width float64
	refs  []*SimElem
}

func (e *SectorElem) Position() db.Vec2 {
	var p db.Vec2
	for _, se := range e.refs {
		p = p.Add(se.S)
	}
	if len(e.refs) == 1 {
		p[1] += 1.1 * e.scale
	} else {
		p = p.Mul(1 / float64(len(e.refs)))
	}
	//p[0] -= e.width/2
	return p
}

func (e *SectorElem) Anchor(p db.Vec2) db.Vec2 {
	d := e.Position().Sub(p)
	return d.Mul(0.5)
}

func (e *SectorElem) Width() float64  { return e.width }
func (e *SectorElem) Weight() float64 { return 2 }

type sect struct {
	label string

	p db.Vec2
	n int
}

func sysfont(fm *FontMap, sys *db.System) *Font {
	if len(sys.Stations) > 0 {
		return fm.Inhabited
	}
	return fm.Empty
}

func trunc(f float64) float64 {
	return math.Floor(f*100+0.5) / 100
}

func trvec(p db.Vec2) (x, y float64) {
	return trunc(p[0]), trunc(p[1])
}

const (
	CubeLyPerStar = 70 // Ly³ there is on the average one star in this volume
	SpaceForStar  = 4  // average on screen area needed for a single star
)

func SphereScale(r float64) float64 {
	vol := 4 / 3 * math.Pi * (r * r * r)
	nfit := vol / CubeLyPerStar
	scale := math.Sqrt(nfit*SpaceForStar) / math.Pi / r
	return scale
}

func BoxScale(xz, y float64) float64 {
	vol := xz * xz * y
	nfit := vol / CubeLyPerStar
	scale := math.Sqrt(nfit*SpaceForStar) / xz / 2.5
	return scale
}
