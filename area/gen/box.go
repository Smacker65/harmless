package gen

import (
	"bitbucket.org/tajtiattila/harmless/db"
)

type box struct {
	l, t, r, b float64 // x: l < r, y: t < b
}

func starbox(p db.Vec2, w, s float64) box {
	return box{p[0] - s/2, p[1] - s/2, p[0] + s*(w+0.5), p[1] + s/2}
}

func intersect(a, b box) bool {
	return a.l < b.r && b.l < a.r && a.t < b.b && b.t < a.b
}

func away(a, b box, sgn float64) (r db.Vec2) {
	if a.l > b.r || b.l > a.r || a.t > b.b || b.t > a.b {
		return v2(0, 0) // no overlap
	}

	// special case for precise y overlap
	if a.t == b.t && a.b == b.b {
		// a.t → b.b or a.b → b.t
		r = db.Vec2{0, sgn * (a.b - a.t)}
	} else {
		// a.t → b.b
		r = db.Vec2{0, b.b - a.t}
		// a.b → b.t
		if d := v2(0, b.t-a.b); d.Abssq() < r.Abssq() {
			r = d
		}
	}
	// a.r → b.l
	if d := v2(b.l-a.r, 0); d.Abssq() < r.Abssq() {
		r = d
	}
	// a.l → b.r
	if d := v2(b.r-a.l, 0); d.Abssq() < r.Abssq() {
		r = d
	}
	return
}

func v2(x, y float64) db.Vec2 { return db.Vec2{x, y} }

func min(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

func max(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}
