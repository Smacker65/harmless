package gen

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"math"
)

const (
	res     = 5
	vstride = 1e9
)

type Grid struct {
	All, work []*SimElem

	c, stride int

	m []gridNode
}

func MakeGrid(sz int) Grid {
	size := sz / res
	m := make([]gridNode, size*size)
	for i := range m {
		n := &m[i]
		n.pred, n.succ = n, n
	}
	return Grid{
		c:      size / 2,
		stride: size,
		m:      m}
}

type gridNode struct {
	pred, succ *gridNode
	data       *SimElem
}

func (g *Grid) Add(e *SimElem) {
	g.All = append(g.All, e)
	th := g.node(e.S)
	n := &gridNode{data: e}
	e.th, e.n = th, n
	th.succ.pred, e.n.succ = e.n, th.succ
	th.succ, e.n.pred = e.n, th
}

func (g *Grid) Move(e *SimElem) {
	th := g.node(e.S)
	if th != e.th {
		e.n.pred.succ = e.n.succ
		e.n.succ.pred = e.n.pred
		e.th = th
		th.succ.pred, e.n.succ = e.n, th.succ
		th.succ, e.n.pred = e.n, th
	}
}

func (g *Grid) Near(p db.Vec2, r float64) []*SimElem {
	xs, ys := g.c+int(math.Floor((p[0]-r)/res)), g.c+int(math.Floor((p[1]-r)/res))
	xe, ye := g.c+int(math.Floor((p[0]+r)/res)), g.c+int(math.Floor((p[1]+r)/res))
	i0 := xs + g.stride*ys
	g.work = g.work[:0]
	for ; ys <= ye; ys++ {
		for i, x := i0, xs; x <= xe; x, i = x+1, i+1 {
			th := &g.m[i]
			for n := th.succ; n != th; n = n.succ {
				g.work = append(g.work, n.data)
			}
		}
		i0 += g.stride
	}
	return g.work
}

func (g *Grid) node(p db.Vec2) *gridNode {
	xs, ys := g.c+int(math.Floor(p[0]/res)), g.c+int(math.Floor(p[1]/res))
	return &g.m[xs+g.stride*ys]
}
