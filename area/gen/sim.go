package gen

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"math"
)

type Simulation struct {
	Grid

	scale    float64
	affector Affector

	av []db.Vec2

	// simulation parameters
	R float64 // radius within to keep elements
}

type Elem interface {
	Position() db.Vec2      // initial position for element at start of simulation
	Anchor(db.Vec2) db.Vec2 // anchor force vector, if any based on current position
	Width() float64         // element width
	Weight() float64        // element weight
}

type SimElem struct {
	Elem

	idx int

	th, n *gridNode

	S db.Vec2 // position
	V db.Vec2 // velocity

	width  float64
	weight float64
}

func NewSimulation(scale float64, a Affector) *Simulation {
	return &Simulation{
		Grid:     MakeGrid(200),
		scale:    scale,
		affector: a,
	}
}

func (sim *Simulation) Add(e Elem) {
	sim.Grid.Add(&SimElem{
		S:      e.Position(),
		width:  e.Width(),
		weight: e.Weight(),
		Elem:   e,
		idx:    len(sim.All),
	})
}

const MaxIter = 5000

func (sim *Simulation) Run() {
	sim.RunIter(MaxIter)
}

func (sim *Simulation) RunIter(n int) {
	sim.RunIterCallback(n, func(int) {})
}

func (sim *Simulation) RunIterCallback(n int, fn func(int)) {
	fn(0)
	for i := 0; !sim.Step() && i < n; i++ {
		fn(i)
	}
}

func (sim *Simulation) Step() bool {
	const (
		rnear   = 20
		kbox    = 10
		kstar   = 1
		kdt     = 0.5
		dmin    = 1e-3
		dmin2   = dmin * dmin
		pushbox = false
	)
	if len(sim.av) < len(sim.All) {
		sim.av = make([]db.Vec2, len(sim.All))
	}
	av := sim.av
	var amax2 float64
	for i, p := range sim.All {
		f := sim.affector.Affect(p.S)
		f = f.Add(p.Anchor(p.S))
		// push away from each other
		for _, q := range sim.Near(p.S, rnear) {
			if p == q {
				continue
			}

			sgn := 1.0
			if q.idx < p.idx {
				sgn = -1
			}

			if pushbox {
				pb, qb := starbox(p.S, p.width, sim.scale), starbox(q.S, q.width, sim.scale)
				f = f.Add(away(pb, qb, sgn).Mul(kstar))
			} else {
				d := p.S.Sub(q.S)
				if math.Abs(d[1]) >= sim.scale || (d[0] < -p.width*sim.scale || d[0] > q.width*sim.scale) {
					continue
				}

				var o float64
				if d.Abssq() < dmin2 {
					o = sgn * sim.scale / dmin2
				} else {
					o = sim.scale / d.Abssq()
				}

				f = f.Add(d.Mul(kstar * o))
			}
		}
		a := f.Mul(1 / p.weight)
		mag := a.Abssq()
		if mag > amax2 {
			amax2 = mag
		}
		av[i] = a
	}

	var vmax2 float64
	for _, e := range sim.All {
		v := e.V.Abssq()
		if vmax2 < v {
			vmax2 = v
		}
	}
	vmax := math.Sqrt(vmax2)

	a := math.Sqrt(amax2)
	finished := a <= 1e-6
	if !finished {
		dta := kdt / a
		dtv := kdt / vmax
		dt := math.Min(dta, dtv)

		for i, p := range sim.All {
			p0 := p.V
			p.V = p.V.Add(av[i].Mul(dt))
			p.S = p.S.Add(p0.Add(p.V).Mul(dt / 2))
			if av[i].Abssq() < 1 {
				p.V = p.V.Mul(0.5)
			}
		}
	}
	return finished
}

type Affector interface {
	Affect(db.Vec2) db.Vec2
}

type noEffect struct{}

func (c *noEffect) Affect(p db.Vec2) (f db.Vec2) { return }

var NoEffect = &noEffect{}

type BoxConstraint struct {
	box
}

func (c *BoxConstraint) Affect(p db.Vec2) (f db.Vec2) {
	// push away from box walls
	const kbox = 10
	if o := -c.l - p[0]; o > 0 {
		f[0] += kbox * o * o
	}
	if o := p[0] - c.r; o > 0 {
		f[0] -= kbox * o * o
	}
	if o := -c.t - p[1]; o > 0 {
		f[1] += kbox * o * o
	}
	if o := p[1] - c.b; o > 0 {
		f[1] -= kbox * o * o
	}
	return
}

type circleConstraint struct {
	c db.Vec2
	r float64
}

func CircleConstraint(c db.Vec2, r float64) Affector { return &circleConstraint{c, r} }

func (c *circleConstraint) Affect(p db.Vec2) (f db.Vec2) {
	d := p.Sub(c.c).Abs()
	if d > c.r {
		u := p.Mul(-1 / d)
		d -= c.r
		f = u.Mul(d * d)
	}
	return
}
