package area

import (
	"bitbucket.org/tajtiattila/harmless/db"
)

type Map struct {
	Scale  float64 `json:"scale"` // scaling to show map on screen
	Hash   string  `json:"-"`     // hash changes when map changes
	Elems  []Sys   `json:"systems"`
	Labels []Label `json:"sectors"`
}

type Sys struct {
	X     float64 `json:"x"`
	Y     float64 `json:"y"`
	Width float64 `json:"w"` // width for click area, assuming font size in FontMap
	Id    string  `json:"id"`
	Label string  `json:"n"` // system name without sector part
}

func (s *Sys) S() db.Vec2 { return db.Vec2{s.X, s.Y} }

type Label struct {
	X     float64 `json:"sx"`
	Y     float64 `json:"sy"`
	Label string  `json:"n"`
}
