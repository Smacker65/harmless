package area

import (
	"bitbucket.org/tajtiattila/harmless/area"
	"bitbucket.org/tajtiattila/harmless/db"
	"bitbucket.org/tajtiattila/harmless/db/dbsql"
	"bytes"
	"database/sql"
	"encoding/gob"
	"fmt"
	"log"
	"sync"
)

const MaxMemoryCache = 100

type AreaStore struct {
	db    *sql.DB
	owned bool // only close owned db

	loadstmt *sql.Stmt
	updstmt  *sql.Stmt
	savestmt *sql.Stmt

	mapper area.Mapper

	mtx   sync.Mutex               // loads mutex
	loads map[*db.System]*loadItem // maps loaded right now
}

type loadItem struct {
	once sync.Once
	am   *area.Map
	err  error
}

func Open(fn string) (*AreaStore, error) {
	sdb, err := dbsql.OpenDB(fn)
	if err != nil {
		return nil, err
	}
	as, err := openDB(sdb, true)
	if err != nil {
		sdb.Close()
	}
	return as, err
}

func OpenIO(src db.IO) (*AreaStore, error) {
	if sdb, ok := src.(*dbsql.SqliteDB); ok {
		return openDB(sdb.DB, false)
	}
	return nil, fmt.Errorf("areasql.OpenIO needs SQL IO")
}

func OpenDB(sdb *sql.DB) (*AreaStore, error) {
	return openDB(sdb, false)
}

func openDB(sdb *sql.DB, owned bool) (*AreaStore, error) {
	ok := false
	loadstmt, err := sdb.Prepare("SELECT map FROM AreaMap WHERE system_id=?;")
	if err != nil {
		log.Println("AreaStore can't prepare loadstmt:", err)
		return nil, err
	}
	defer func() {
		if !ok {
			loadstmt.Close()
		}
	}()

	updstmt, err := sdb.Prepare("UPDATE AreaMap SET accessed=CURRENT_TIMESTAMP WHERE system_id=?;")
	if err != nil {
		log.Println("AreaStore can't prepare updstmt:", err)
		return nil, err
	}
	defer func() {
		if !ok {
			updstmt.Close()
		}
	}()

	savestmt, err := sdb.Prepare("INSERT OR REPLACE INTO AreaMap(system_id,hash,map) VALUES(?,?,?);")
	if err != nil {
		log.Println("AreaStore can't prepare savestmt:", err)
		return nil, err
	}

	ok = true
	return &AreaStore{
		db:       sdb,
		owned:    owned,
		loadstmt: loadstmt,
		updstmt:  updstmt,
		savestmt: savestmt,
		loads:    make(map[*db.System]*loadItem),
	}, nil
}

func (as *AreaStore) Close() error {
	var err error
	if as.loadstmt != nil {
		err = as.loadstmt.Close()
	}
	if as.updstmt != nil {
		if errx := as.updstmt.Close(); err == nil {
			err = errx
		}
	}
	if as.savestmt != nil {
		if errx := as.savestmt.Close(); err == nil {
			err = errx
		}
	}
	if as.owned {
		if errx := as.db.Close(); errx != nil {
			return errx
		}
	}
	return err
}

func (as *AreaStore) SetMapper(m area.Mapper) {
	as.mapper = m
}

func (as *AreaStore) Mapper() area.Mapper {
	return as.mapper
}

func (as *AreaStore) Hashes(data *db.Data, fn func(sys *db.System, hash string) error) error {
	rows, err := as.db.Query("SELECT system_id, hash FROM AreaMap;")
	if err != nil {
		return err
	}
	for rows.Next() {
		var (
			id   int64
			hash string
		)
		if err = rows.Scan(&id, &hash); err != nil {
			return err
		}
		if sys := data.DbSystem[id]; sys != nil {
			if err = fn(sys, hash); err != nil {
				return err
			}
		}
	}
	return nil
}

func (as *AreaStore) Load(sys *db.System) (*area.Map, error) {
	as.mtx.Lock()
	ci := as.loads[sys]
	if ci == nil {
		ci = new(loadItem)
		as.loads[sys] = ci
	}
	as.mtx.Unlock()

	// allow only one sqlite or generator access
	// at the same time for the same system
	ci.once.Do(func() {
		ci.am, ci.err = as.load(sys)
		as.mtx.Lock()
		delete(as.loads, sys)
		as.mtx.Unlock()
	})

	return ci.am, ci.err
}

func (as *AreaStore) load(sys *db.System) (am *area.Map, err error) {
	if sys.DbKey == 0 {
		return nil, fmt.Errorf("System.DbKey of %s is zero (areasql.AreaStore needs SQL backed Data)", sys.Id)
	}

	var tx *sql.Tx
	tx, err = as.db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Commit()

	var data []byte
	if err = tx.Stmt(as.loadstmt).QueryRow(sys.DbKey).Scan(&data); err == nil {
		if _, xerr := tx.Stmt(as.updstmt).Exec(sys.DbKey); xerr != nil {
			log.Println("AreaStore.Load can't update access time:", err)
		}
		buf := bytes.NewBuffer(data)
		var xam area.Map
		if err = gob.NewDecoder(buf).Decode(&xam); err != nil {
			log.Println("AreaStore.Load can't decode map BLOB:", err)
			return nil, err
		}
		am = &xam
	} else {
		if err != sql.ErrNoRows {
			log.Println("AreaStore.Load can't scan sqlite result", err)
			return nil, err
		}
		if as.mapper == nil {
			return nil, area.ErrMissing(sys)
		}
		am = as.mapper.Map(sys.P)
		err = as.save(tx, sys, am)
	}
	return
}

func (as *AreaStore) Save(sys *db.System, am *area.Map) (err error) {
	var tx *sql.Tx
	tx, err = as.db.Begin()
	if err != nil {
		log.Println("AreaStore.Save can't begin tx:", err)
		return err
	}
	defer tx.Commit()

	return as.save(tx, sys, am)
}

func (as *AreaStore) save(tx *sql.Tx, sys *db.System, am *area.Map) (err error) {
	var buf bytes.Buffer
	if err = gob.NewEncoder(&buf).Encode(am); err != nil {
		return
	}

	_, err = tx.Stmt(as.savestmt).Exec(sys.DbKey, am.Hash, buf.Bytes())
	return
}

type sqliteDriver struct{}

func (*sqliteDriver) Open(fn string) (area.Store, error)   { return Open(fn) }
func (*sqliteDriver) OpenIO(src db.IO) (area.Store, error) { return OpenIO(src) }

func init() {
	area.RegisterDriver("sqlite", new(sqliteDriver))
}
