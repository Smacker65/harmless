package area

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"fmt"
)

type Store interface {
	// Load loads the map for sys
	Load(sys *db.System) (*Map, error)

	// Save stores the map for sys
	Save(sys *db.System, m *Map) error

	// Hashes list stored hashes for systems in data
	Hashes(data *db.Data, fn func(sys *db.System, hash string) error) error

	Close() error

	// SetMapper sets the Mapper the Store should in Load use if no cached map is available.
	// The Store should store the generated map for later use.
	SetMapper(m Mapper)

	// Mapper returns the Mapper of the Store, if any
	Mapper() Mapper
}

type Mapper interface {
	// Map generates a map around centre
	Map(centre db.Vec3) *Map

	// MapIterCallback generates a map around centre up until maxiter,
	// falling f for each map generated while iterating. If f is nil,
	// no intermediate maps will be generated.
	MapIter(centre db.Vec3, maxiter int, f func(*Map)) *Map
}

type errMissing struct {
	*db.System
}

func (err *errMissing) Error() string {
	return fmt.Sprintf("map for %s:%d is missing", err.Id, err.DbKey)
}

func ErrMissing(sys *db.System) error {
	return &errMissing{sys}
}

// IsMissing checks whether an error returned by Load means the
// system has no map in the store.
func IsMissing(err error) bool {
	_, ok := err.(*errMissing)
	return ok
}

func Open(driver, name string) (Store, error) {
	if drv, ok := drivers[driver]; ok {
		return drv.Open(name)
	}
	return nil, fmt.Errorf(`area.Driver "%s" is not available`, driver)
}

func OpenIO(driver string, pio db.IO) (Store, error) {
	if drv, ok := drivers[driver]; ok {
		return drv.OpenIO(pio)
	}
	return nil, fmt.Errorf(`area.Driver "%s" is not available`, driver)
}

type Driver interface {
	Open(fn string) (Store, error)
	OpenIO(pio db.IO) (Store, error)
}

var drivers = make(map[string]Driver)

func RegisterDriver(name string, d Driver) {
	if _, ok := drivers[name]; ok {
		panic("driver " + name + " is already registered")
	}
	drivers[name] = d
}

// NoStore is a Store that generates maps on the fly
type NoStore struct {
	m Mapper
}

func NewNoStore(m Mapper) *NoStore {
	return &NoStore{m}
}

func (s *NoStore) Load(sys *db.System) (*Map, error) {
	if s.m == nil {
		return nil, fmt.Errorf("using NoStore without setting Mapper")
	}
	return s.m.Map(sys.P), nil
}

func (s *NoStore) Save(sys *db.System, m *Map) error {
	return fmt.Errorf("NoStore does not support storing maps")
}

func (s *NoStore) Hashes(data *db.Data, fn func(sys *db.System, hash string) error) error {
	return fmt.Errorf("NoStore does not support retrieving hashes of maps")
}

func (s *NoStore) Close() error { return nil }

func (s *NoStore) SetMapper(m Mapper) {
	s.m = m
}

func (s *NoStore) Mapper() Mapper {
	return s.m
}
