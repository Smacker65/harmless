package main

import (
	"bitbucket.org/tajtiattila/harmless/area"
	_ "bitbucket.org/tajtiattila/harmless/area/areasql"
	"bitbucket.org/tajtiattila/harmless/db"
	_ "bitbucket.org/tajtiattila/harmless/db/dbsql"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

type ChartData struct {
	XChart `json:"chart"`
	Error  string `json:"error,omitempty"`
}

var chartcache = make(map[*db.System][]*area.Map)

func ServeChart(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("chart", req).Enter()
	defer errh.Exit()

	cstar := db.IdSystem(req.FormValue("c"))
	if cstar == nil {
		errh.Err(ServeJson(w, req, ChartData{Error: fmt.Sprintf("c=%#v invalid", req.FormValue("c"))}))
		return
	}

	radius := DefaultRadius
	if rs := req.FormValue("r"); rs != "" {
		if rx, err := strconv.ParseFloat(rs, 64); err == nil {
			radius = rx
		}
	}

	jumprange, _ := strconv.ParseFloat(req.FormValue("jr"), 64)

	iter := -1
	is := req.FormValue("i")
	if is != "" {
		if i, err := strconv.Atoi(is); err == nil {
			iter = i
		}
	}

	chart, err := GenerateChart(AreaStore, cstar, radius, jumprange, iter)
	if err != nil {
		errh.Err(ServeJson(w, req, ChartData{Error: err.Error()}))
	}
	errh.Err(ServeJson(w, req, chart))
}

var clstrc ClusterCache

func GenerateChart(as area.Store, cstar *db.System, radius, jumprange float64, iter int) (*ChartData, error) {
	if cstar == nil {
		return nil, fmt.Errorf("system must not be nil")
	}
	var (
		am  *area.Map
		err error
	)
	if iter >= 0 {
		if len(chartcache[cstar]) < iter {
			v := make([]*area.Map, 0, iter+1)
			as.Mapper().MapIter(cstar.P, iter, func(am *area.Map) {
				if len(v)%100 == 99 {
					fmt.Print(".")
				}
				v = append(v, am)
			})
			fmt.Println()
			for len(v) < iter+1 {
				v = append(v, v[len(v)-1])
			}
			chartcache[cstar] = v
		}
		am = chartcache[cstar][iter]
	}

	if am == nil {
		am, err = as.Load(cstar)
		if err != nil {
			return nil, err
		}
	}

	chart := XChart{
		Scale:  am.Scale,
		Elems:  make([]XSys, 0, len(am.Elems)),
		Labels: make([]area.Label, 0, len(am.Labels)),
	}

	r2 := radius * radius

	fclstr := func(*db.System) int { return 0 }
	if jumprange != 0 {
		cm := clstrc.Map(jumprange)
		fclstr = func(sys *db.System) int {
			return cm[sys]
		}
	}
	labels := make(map[string]bool)
	for _, e := range am.Elems {
		sys := db.IdSystem(e.Id)
		if sys == nil {
			continue
		}
		if sys.P.Sub(cstar.P).Abssq() > r2 {
			continue
		}
		if sect, _ := db.SplitLabel(sys.Name); sect != "" {
			labels[strings.ToLower(sect)] = true
		}
		chart.Elems = append(chart.Elems, XSys{
			Sys:     sys,
			Cluster: fclstr(sys),
			X:       e.X,
			Y:       -e.Y, // swap sign for proper screen alignment
			Width:   e.Width,
			Label:   e.Label,
		})
	}
	for _, sect := range am.Labels {
		if labels[strings.ToLower(sect.Label)] {
			sect.Y *= -1 // swap sign for proper screen alignment
			chart.Labels = append(chart.Labels, sect)
		}
	}

	return &ChartData{XChart: chart}, nil
}

type XChart struct {
	Scale  float64      `json:"scale"`
	Elems  []XSys       `json:"systems"`
	Labels []area.Label `json:"sectors"`
}

type XSys struct {
	Sys *db.System `json:"sys"`

	Cluster int     `json:"clstr,omitempty"`
	X       float64 `json:"sx"`
	Y       float64 `json:"sy"`
	Width   float64 `json:"w"` // width for click area, assuming font size in FontMap
	Label   string  `json:"n"` // system name without sector part
}

func (s *XSys) S() db.Vec2 { return db.Vec2{s.X, s.Y} }
