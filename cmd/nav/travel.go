package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	_ "bitbucket.org/tajtiattila/harmless/db/dbsql"
	"bitbucket.org/tajtiattila/harmless/route"
	"flag"
	"fmt"
	"os"
)

func main() {
	ly := flag.Float64("ly", 6.96, "jump range")
	fueltank := flag.Float64("tank", 2.0, "fuel tank size [t]")
	maxjumpfuel := flag.Float64("jf", 0.8, "max. jump fuel [t]")
	pow := flag.Float64("pow", 2.0, "jump drive exponent value")
	dbf := flag.String("db", "./data", "path to database")
	quiet := flag.Bool("q", false, "quiet mode (hide header and legend)")
	line := flag.Bool("line", false, "show route(s) on single line (implies quiet)")
	savef := flag.Bool("fuel", false, "save fuel")
	savet := flag.Bool("time", false, "save time")
	flag.Parse()

	jr := route.NewSimpleJumper(*fueltank, *maxjumpfuel, *ly)
	jr.FuelPower = *pow
	if *savef || *savet {
		jr.ScoopFuel = false
		if *savef {
			jr.Evaluator = route.OptFuelUse
		} else {
			jr.Evaluator = route.OptTime
		}
	} else {
		jr.ScoopFuel = true
		jr.Evaluator = route.OptTime
	}

	check(db.LoadDir(*dbf))

	args := flag.Args()
	if len(args) < 2 {
		fmt.Fprintln(os.Stderr, "needs at least 2 args (start and destination system)")
		os.Exit(1)
	}
	shownany := false
	for i := range args {
		j := i + 1
		if j < len(args) {
			a, err := db.FindSystem(args[i])
			check(err)
			b, err := db.FindSystem(args[j])
			check(err)
			r := route.AstarRoute(db.DefaultData.Chart, jr, a, b)
			if r != nil {
				if *line {
					fmt.Println(r)
				} else {
					if !*quiet {
						fmt.Printf("route from %s to %s:\n", a.Name, b.Name)
					}
					route.PrintRoute(r)
					shownany = true
				}
			} else {
				fmt.Println("!", b.Name, "is unreachable from", a.Name)
			}
		}
	}
	if shownany && !*quiet {
		fmt.Println(" Legend: ", route.RouteLegend)
	}
}

func check(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
