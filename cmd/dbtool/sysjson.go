package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"
)

type SysJsonSys struct {
	Name string `json:"name"`

	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`

	Stations []sjStation `json:"stations,omitempty"`

	Contr    string    `json:"contributor,omitempty"`
	Region   string    `json:"region,omitempty"`
	Modified time.Time `json:"contributed,omitempty"`

	Allegiance string
	Economy    string
	Government string
	Population int64
	Security   string
}

type sjStation struct {
	Name string  `json:"station"`
	Dist float64 `json:"distance,omitempty"`

	Type     string      `json:"type,omitempty"`
	Shipyard interface{} `json:"shipyard,omitempty"`
	Outfit   interface{} `json:"outfitting,omitempty"`
	Cmarket  interface{} `json:"commodities_market,omitempty"`
	Bmarket  interface{} `json:"black_market,omitempty"`

	Faction string `json:"faction,omitempty"`
	PadSize string `json:"largest_pad,omitempty"`
}

type readSysJson struct {
	fn string
}

func (o *readSysJson) Title() string { return fmt.Sprintf("· systems-json ‘%s’", o.fn) }

func (o *readSysJson) Run(c *Context) error {
	f, err := os.Open(o.fn)
	if err != nil {
		return err
	}
	defer f.Close()

	var v []SysJsonSys
	if err = json.NewDecoder(f).Decode(&v); err != nil {
		return err
	}

	data := c.Top()
	data.Systems = data.Systems[:0]
	for _, js := range v {
		if strings.HasSuffix(strings.ToLower(js.Name), "(removed)") {
			continue
		}
		const unvertag = "(unverified)"
		if strings.HasSuffix(strings.ToLower(js.Name), unvertag) {
			js.Name = strings.TrimSpace(strings.TrimSuffix(js.Name, unvertag))
		}
		added, valid := o.addedValidValues(js.Contr, js.Region)
		sys := &db.System{
			Name:     js.Name,
			P:        db.Vec3{js.X, js.Y, js.Z},
			Added:    added,
			Valid:    valid,
			Modified: js.Modified,
		}
		if js.Allegiance != "" {
			sys.SystemDetail = &db.SystemDetail{
				Alleg: js.Allegiance,
				Econ:  fixecon(js.Economy),
				Govt:  js.Government,
				Pop:   js.Population,
				Sec:   db.ParseSecurity(js.Security),
				Valid: valid,
			}
		}
		for _, jt := range js.Stations {
			stn := db.Station{
				Name: jt.Name,
				Dist: jt.Dist,
				StationDetail: db.StationDetail{
					Type:     jt.Type,
					Faction:  jt.Faction,
					Shipyard: sysjbool(jt.Shipyard),
					Outfit:   sysjbool(jt.Outfit),
					Cmarket:  sysjbool(jt.Cmarket),
					Bmarket:  sysjbool(jt.Bmarket),
				},
				Valid: valid,
			}

			sys.Stations = append(sys.Stations, stn)
		}
		data.Systems = append(data.Systems, sys)
	}
	return nil
}

func (o *readSysJson) addedValidValues(contr, reg string) (added string, valid db.Validity) {
	xadd, remark := reg, ""
	if n := strings.IndexRune(reg, '('); n != -1 {
		xadd, remark = strings.TrimSpace(reg[:n]), strings.TrimSpace(reg[n+1:])
		if len(remark) != 0 && remark[len(remark)-1] == ')' {
			remark = remark[:len(remark)-1]
		}
	}
	added = xadd
	if strings.HasPrefix(xadd, "Gamma") ||
		strings.HasPrefix(xadd, "Beta") ||
		strings.HasPrefix(xadd, "Premium Beta") ||
		strings.HasPrefix(xadd, "Alpha") {
		if remark == "unverified" {
			valid = db.ParseValidity(xadd)
		} else {
			valid = db.Current
		}
	} else {
		valid = db.Invalid
	}
	return
}

func popsfx(s *string, sfx string) bool {
	if strings.HasSuffix(strings.ToLower(*s), sfx) {
		*s = strings.TrimSpace((*s)[:len(*s)-len(sfx)])
		return true
	}
	return false
}

func fixecon(s string) string {
	if s == "Agricultural" {
		return "Agriculture"
	}
	return s
}

func sysjbool(i interface{}) *bool {
	if b, ok := i.(bool); ok {
		return &b
	}
	return nil
}
