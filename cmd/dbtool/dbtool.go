package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func main() {
	argv := os.Args[1:]
	if len(argv) == 0 {
		verify(new(errHelp))
	}

	vop, err := ParseArgs(argv)
	verify(err)

	c := Context{Stack: []StackElem{{}}}
	for _, op := range vop {
		fmt.Println(op.Title())
		err = op.Run(&c)
		verify(err)
	}
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func ParseArgs(argv []string) (v []Op, err error) {
	for i := 0; i < len(argv); {
		cmd := argv[i]
		if cmd == "" {
			return nil, fmt.Errorf("empty command")
		}
		if cmd[0] != '-' {
			return nil, fmt.Errorf("‘%s’ unexpected", cmd)
		}
		var op Op
		for _, spec := range OpSpecs {
			if spec.Name == cmd[1:] {
				i++
				narg := len(spec.Args)
				if len(argv[i:]) < narg {
					return nil, fmt.Errorf("not enough arguments for ‘%s’ (%d needed)", cmd, narg)
				}
				cmdargs := argv[i : i+narg]
				i += narg
				switch f := spec.New.(type) {
				case func() Op:
					op = f()
				case func() (Op, error):
					op, err = f()
				case func([]string) Op:
					op = f(cmdargs)
				case func([]string) (Op, error):
					op, err = f(cmdargs)
				default:
					panic(fmt.Errorf("cmd ‘%s’ invalid", cmd))
				}
				if err != nil {
					return nil, err
				}
			}
		}
		if op == nil {
			return nil, fmt.Errorf("unknown command ‘%s’", cmd)
		}
		v = append(v, op)
	}
	return
}

type errHelp struct{}

func (*errHelp) Error() string {
	w := new(bytes.Buffer)
	fmt.Fprintf(w, `usage: %s [ -command [ cmdargs... ] ]...
available commands:
`, filepath.Base(os.Args[0]))
	maxl := 0
	for _, op := range OpSpecs {
		l := len(op.Name)
		for _, a := range op.Args {
			l += len(a) + 1
		}
		if l > maxl {
			maxl = l
		}
	}
	for _, op := range OpSpecs {
		cmd := op.Name
		if len(op.Args) != 0 {
			cmd += " " + strings.Join(op.Args, " ")
		}
		fmt.Fprintf(w, "  -%-*s  %s\n", maxl, cmd, op.Help)
	}
	return w.String()
}

type OpSpec struct {
	Name string
	Args []string
	Help string
	New  interface{}
}

var OpSpecs = []OpSpec{
	{"help", nil, "show help", func() (Op, error) { return nil, new(errHelp) }},
	{"v", nil, "set verbose mode", func() Op { return new(verboseOp) }},
	{"j", varg("n"), "set # of worker therads", func(a []string) (Op, error) {
		n, err := strconv.Atoi(a[0])
		if err != nil {
			return nil, err
		}
		return &numThreadsOp{n}, nil
	}},

	{"push", nil, "pushes active data down the stack", func() Op { return new(pushOp) }},
	{"pop", nil, "pops the top data off the stack", func() Op { return new(popOp) }},
	{"stat", nil, "show statistics in verbose mode", func() Op { return new(statOp) }},
	{"newsys", nil, "replaces systems at top of stack with the ones down below", func() Op { return &newSysOp{false} }},
	{"updsys", nil, "update systems at top of stack with the ones down below", func() Op { return &newSysOp{true} }},

	{"inhabit", nil, `adds missing stations to the data on top of the stack
from the one below, assuming all systems in the source have a station`, func() Op { return new(inhabitOp) }},

	{"mergepos", nil, `merge systems at the same position`, func() Op { return new(mergePosOp) }},

	{"signif", nil, "calc significance", func() Op { return new(signifOp) }},
	{"shift", varg("x", "y", "z"), "shifts coordinates", func(a []string) (op Op, err error) {
		var x, y, z float64
		if x, err = strconv.ParseFloat(a[0], 64); err != nil {
			return
		}
		if y, err = strconv.ParseFloat(a[1], 64); err != nil {
			return
		}
		if z, err = strconv.ParseFloat(a[2], 64); err != nil {
			return
		}
		return shiftOp{x, y, z}, nil
	}},

	{"added", varg("added", "modified"), "sets added and modified parameters for all systems",
		func(a []string) (Op, error) {
			ts, err := time.Parse(time.RFC3339, a[1])
			if err != nil {
				return nil, err
			}
			return &addedOp{a[0], ts}, nil
		},
	},

	{"editlog", varg("filename"), "apply edit log file", func(a []string) (Op, error) {
		return &editLogOp{a[0]}, checkf(a[0])
	}},

	{"rjson", varg("filename"), "read json data", func(a []string) (Op, error) {
		return &readOp{"json", a[0]}, checkf(a[0])
	}},

	{"rsql", varg("filename"), "read sqlite data", func(a []string) (Op, error) {
		return &readOp{"sqlite", a[0]}, checkf(a[0])
	}},

	{"tddb", varg("filename"), "read TradeDangerous sqlite data", func(a []string) (Op, error) {
		return &tdexp{a[0]}, checkf(a[0])
	}},

	{"rtxt", varg("filename"), "read flat text system data", func(a []string) (Op, error) {
		return &textDataOp{a[0]}, checkf(a[0])
	}},

	{"sysjson", varg("filename"), "read systems.json file", func(a []string) (Op, error) {
		return &readSysJson{a[0]}, checkf(a[0])
	}},

	{"wjson", varg("filename"), "write json data", func(a []string) Op {
		return &writeOp{"json", a[0]}
	}},

	{"wsql", varg("filename"), "write sqlite data", func(a []string) Op {
		return &writeOp{"sqlite", a[0]}
	}},
}

func varg(s ...string) []string {
	return s
}

func checkf(fn string) error {
	_, err := os.Stat(fn)
	if err != nil {
		fmt.Printf("warning: %s\n", err)
	}
	return nil
}
