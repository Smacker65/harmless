package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	_ "bitbucket.org/tajtiattila/harmless/db/dbsql"
	"encoding/json"
	"io"
	"os"
)

// editLogOp writes changes written in an edit log to the current data
type editLogOp struct {
	fn string
}

func (o *editLogOp) Title() string { return "· apply edit log" }

func (o *editLogOp) Run(c *Context) error {
	f, err := os.Open(o.fn)
	if err != nil {
		return err
	}
	defer f.Close()

	src := json.NewDecoder(f)

	data := &c.Top().Data

	for {
		var entry editLogEntry
		err = src.Decode(&entry)
		if err != nil {
			break
		}
		sys := entry.Data
		if sys == nil {
			continue
		}
		oldsys := data.IdSystem(sys.Id)
		if oldsys == nil {
			oldsys, err = data.FindSystem(sys.Name)
			if err != nil {
				c.Errorf("can't find system: Id=%q Name=%q\n", sys.Id, sys.Name)
				continue
			}
		}
		if oldsys.P != sys.P {
			c.Errorf("coordinate mismatch: Id=%q old.P=%v new.P=%v\n", sys.Id, oldsys.P, sys.P)
			continue
		}
		if c.Verbose {
			c.Printf("  > %s %s@%s\n", oldsys.Id, entry.Cmdr, entry.RemoteAddr)
			switch {
			case sys.SystemDetail != nil && oldsys.SystemDetail == nil:
				c.Println("  + detail")
			case sys.SystemDetail != nil && oldsys.SystemDetail != nil:
				if *sys.SystemDetail != *oldsys.SystemDetail {
					c.Println("  ● detail")
				}
			case sys.SystemDetail == nil && oldsys.SystemDetail != nil:
				c.Println("  - detail")
			}
		}
		sys.Id = oldsys.Id
		*oldsys = *sys
	}

	data.CalcSystemIds()
	data.CalcSysStationIds()

	if err == io.EOF {
		return nil
	}
	return err
}

type editLogEntry struct {
	Cmdr       string     `json:"cmdr"`
	Data       *db.System `json:"data"`
	RemoteAddr string     `json:"remoteAddr"`
}
