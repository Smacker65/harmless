package main

import (
	"bitbucket.org/tajtiattila/harmless/market/tdprice"
	"flag"
	"fmt"
	"os"
)

func main() {
	json := flag.String("prices", "TradeDangerous.prices", "prices file to load")
	data := flag.String("data", "./data", "path to data files")
	flag.Parse()

	f, err := os.Open(*json)
	if err == nil {
		defer f.Close()
		err = tdprice.UpdateDb(*data+"/elite.sqlite", f)
	}

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
