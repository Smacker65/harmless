package main

import (
	. "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/market"
	_ "bitbucket.org/tajtiattila/harmless/market/json"
	_ "bitbucket.org/tajtiattila/harmless/market/sqlitedb"
	"bitbucket.org/tajtiattila/harmless/planner"
	"bitbucket.org/tajtiattila/harmless/util"
	"os"
)

func main() {
	cfg := NewConfig()

	mds, err := market.Open("sqlite", util.PreparePath(cfg.DB))
	if err != nil {
		panic(err)
	}
	defer mds.Close()

	trader := planner.NewTrader(cfg.Config)
	result := trader.Query(mds, &cfg.Query, nil)

	if cfg.Html {
		if err = htmlTmpl.Execute(os.Stdout, result); err != nil {
			panic(err)
		}
	} else {
		if err = textFmt.Execute(os.Stdout, result); err != nil {
			panic(err)
		}
	}
}
