package htmlsup

import (
	"log"
	"net/http"
	"time"
)

// hlog is a logger for HTTP requests
type Hlogger struct {
	start  time.Time
	prefix string
	raddr  string
	url    string
}

func Hlog(fn string, req *http.Request) Hlogger {
	return Hlogger{time.Now(), fn + ":", RemoteAddr(req), req.URL.String()}
}

func (h Hlogger) Enter() Hlogger {
	log.Println(h.raddr, ">", h.prefix, h.url)
	return h
}

func (h *Hlogger) Exit() error {
	log.Println(h.raddr, "<", h.prefix, h.url, time.Now().Sub(h.start))
	return nil
}

func (h *Hlogger) Err(err error) {
	if err != nil {
		log.Println(h.raddr, "!", h.prefix, h.url)
		log.Println(err)
	}
}

func RemoteAddr(req *http.Request) string {
	if proxy_ip := req.Header.Get("X-Real-IP"); proxy_ip != "" {
		return proxy_ip
	}
	return req.RemoteAddr
}
