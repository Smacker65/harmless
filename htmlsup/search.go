package htmlsup

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

type SearchResult struct {
	Label string `json:"label"`
	Value srev   `json:"value"`
}

type srev struct {
	*db.System
	Dist float64 `json:"dist"`
}

func SystemSearch(query string, c *db.System) (v []SearchResult) {
	if len(query) < 3 {
		return nil
	}
	query = strings.ToLower(query)
	for _, s := range db.Systems {
		if q := matchname(query, s.Name); q > 0 {
			var (
				d float64
			)
			if c != nil {
				d = trunc3(s.P.Sub(c.P).Abs())
			}
			v = append(v, SearchResult{
				Label: s.Name,
				Value: srev{
					System: s,
					Dist:   d,
				},
			})
		}
	}
	sort.Sort(distsort(v))
	if len(v) > 20 {
		v = v[:20]
	}
	return
}

func matchname(query, value string) int {
	v, minq := value, int(1e9)
	for v != "" {
		q := mq(query, v)
		if q < minq {
			minq = q
		}
		_, vs := utf8.DecodeRuneInString(v)
		v = v[vs:]
	}
	return len(value) - minq
}

func mq(query, value string) int {
	p, gap := 0, 0
	for _, qr := range query {
		p0, skip := p, false
		for {
			vr, vs := utf8.DecodeRuneInString(value[p:])
			if vs == 0 {
				return 1e9
			}
			p += vs
			if unicode.ToLower(vr) == qr {
				break
			}
			if qr == ' ' && unicode.In(vr, unicode.Space, unicode.Punct) {
				break
			}
			if unicode.In(vr, unicode.Number, unicode.Letter) {
				skip = true
			}
		}
		if p0 != 0 && skip {
			gap++
		}
	}
	return gap
}

func lowercaseless(a, b string) bool {
	for a != "" && b != "" {
		ar, as := utf8.DecodeRuneInString(a)
		br, bs := utf8.DecodeRuneInString(b)
		al, bl := unicode.ToLower(ar), unicode.ToLower(br)
		if al != bl {
			return al < bl
		}
		a, b = a[as:], b[bs:]
	}
	return len(a) < len(b)
}

type distsort []SearchResult

func (v distsort) Len() int      { return len(v) }
func (v distsort) Swap(i, j int) { v[i], v[j] = v[j], v[i] }

func (v distsort) Less(i, j int) bool {
	f := v[i].Value.Dist - v[j].Value.Dist
	switch {
	case f < 0:
		return true
	case f > 0:
		return false
	}
	return lowercaseless(v[i].Label, v[j].Label)
}

func trunc3(v float64) float64 {
	return float64(int(v*1000+0.5)) / 1000
}
