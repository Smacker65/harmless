package htmlsup

import (
	"compress/gzip"
	"encoding/json"
	"io"
	"net/http"
	"strings"
)

func NewJsonWriter(w http.ResponseWriter, req *http.Request) io.Writer {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if strings.Contains(req.Header.Get("Accept-Encoding"), "gzip") {
		w.Header().Set("Content-Encoding", "gzip")
		return gzip.NewWriter(w)
	}
	return w
}

func ServeJson(w http.ResponseWriter, req *http.Request, data interface{}) error {
	ww := NewJsonWriter(w, req)
	defer Close(ww)
	return json.NewEncoder(ww).Encode(data)
}

func Close(i interface{}) {
	if c, ok := i.(io.Closer); ok {
		c.Close()
	}
}
