package emdn

import (
	"io"
)

type mwc struct {
	v []io.WriteCloser
}

func (t *mwc) Write(p []byte) (n int, err error) {
	for _, w := range t.v {
		n, err = w.Write(p)
		if err != nil {
			return
		}
		if n != len(p) {
			err = io.ErrShortWrite
			return
		}
	}
	return len(p), nil
}

func (t *mwc) Close() (err error) {
	for _, w := range t.v {
		if e := w.Close(); err != nil {
			err = e
		}
	}
	return
}

// MultiWriterCloser creates a writecloser that duplicates its writes to all the
// provided writers, similar to the Unix tee(1) command.
func multiWriterCloser(v ...io.WriteCloser) io.WriteCloser {
	if len(v) != 0 {
		w := make([]io.WriteCloser, len(v))
		copy(w, v)
		return &mwc{w}
	}
	return new(nopwc)
}

type nopwc struct{}

func (*nopwc) Write(p []byte) (n int, err error) { return len(p), nil }
func (*nopwc) Close() error                      { return nil }
