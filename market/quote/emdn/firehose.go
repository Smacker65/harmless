package emdn

import (
	"bitbucket.org/tajtiattila/harmless/market/quote"
	"bytes"
	"compress/zlib"
	"encoding/json"
	"fmt"
	zmq "github.com/pebbe/zmq4"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

func decompress(in string) (out []byte, err error) {
	r, err := zlib.NewReader(bytes.NewBufferString(in))
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(r)
}

type Firehose struct {
	sock *zmq.Socket
	dump io.WriteCloser
}

func NewFirehose(url string, dump io.WriteCloser) (*Firehose, error) {
	return NewFirehoseDump(url, multiWriterCloser())
}

func NewFirehoseDump(url string, dump io.WriteCloser) (*Firehose, error) {
	sock, err := zmq.NewSocket(zmq.SUB)
	if err != nil {
		return nil, err
	}

	if err = sock.Connect(url); err != nil {
		return nil, err
	}
	if err = sock.SetSubscribe(""); err != nil {
		return nil, err
	}

	return &Firehose{sock, dump}, nil
}

func (f *Firehose) Read(mq *quote.Quote) error {
	for {
		cdata, err := f.sock.Recv(0)
		if err != nil {
			return err
		}
		data, err := decompress(cdata)
		if err != nil {
			return err
		}
		fmt.Fprintf(f.dump, "%s\n", data)
		var m Message
		err = json.Unmarshal(data, &m)
		if err != nil {
			return err
		}

		if mqc, ok := m.Content.(*MarketQuoteContent); ok {
			*mq = mqc.Quote
			return nil
		}
	}
	return nil // not reached
}

func (f *Firehose) Close() error {
	f.dump.Close()
	return f.sock.Close()
}

func init() {
	quote.Register("firehose", func(url string) (quote.Source, error) {
		fields := strings.Fields(url)
		if len(fields) < 1 {
			return nil, fmt.Errorf("firehose: missing url")
		}
		url = fields[0]
		var wv []io.WriteCloser
		for _, opt := range fields[1:] {
			if len(opt) > 4 && opt[:4] == "log=" {
				logf := opt[4:]
				w, err := os.OpenFile(logf, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
				if err != nil {
					return nil, err
				}
				wv = append(wv, w)
			}
		}
		return NewFirehoseDump(url, multiWriterCloser(wv...))
	})
}
