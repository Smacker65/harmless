package market

import (
	"bitbucket.org/tajtiattila/harmless"
	"time"
)

type Commodity struct {
	Name     string
	Category string
}

// Data represents all data in the market known to the source
type Data struct {
	Commodities []*CommodityData
	Stations    []*StationData
}

type Price int

// PriceData represents commodity price statistics
type PriceData struct {
	Timestamp time.Time
	Buy, Sell PriceInfo
}

// PriceInfo represents either sell or buy statistics
type PriceInfo struct {
	Price                 Price     // buy or sell price
	Qty                   int       // stock or demand
	Hourly, Daily, Weekly MinMaxAvg // stats for buy or sell price
}

type MinMaxAvg struct {
	Min, Avg, Max Price
}

type StationData struct {
	*harmless.Station
	Commodities []StationEntry
}

type CommodityData struct {
	Commodity
	Stations []CommodityEntry
}

func (c *CommodityData) At(s *harmless.System) *PriceData {
	for _, p := range c.Stations {
		if p.Station.System == s {
			return p.PriceData
		}
	}
	return nil
}

func (c *CommodityData) BuyAt(s *harmless.System) *PriceInfo {
	if d := c.At(s); d != nil {
		return &d.Buy
	}
	return nil
}

func (c *CommodityData) SellAt(s *harmless.System) *PriceInfo {
	if d := c.At(s); d != nil {
		return &d.Sell
	}
	return nil
}

type StationEntry struct {
	*CommodityData
	*PriceData
}

type CommodityEntry struct {
	*StationData
	*PriceData
}
