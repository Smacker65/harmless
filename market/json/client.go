package marketjson

import (
	"bitbucket.org/tajtiattila/harmless/market"
	"compress/zlib"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

type Client struct {
	url  string
	data *market.Data
	err  error
	next time.Time
}

func NewClient(url string) (*Client, error) {
	c := &Client{url: url}
	c.runquery()
	if c.err != nil {
		return nil, c.err
	}
	return c, nil
}

func (c *Client) Query() (*market.Data, error) {
	if c.next.Before(time.Now()) {
		c.runquery()
	}
	return c.data, c.err
}

func (c *Client) Close() error {
	return nil
}

func (c *Client) runquery() {
	resp, err := http.Get(c.url)
	if err != nil {
		c.data, c.err = nil, err
		return
	}
	defer resp.Body.Close()
	z, err := zlib.NewReader(resp.Body)
	if err != nil {
		c.data, c.err = nil, err
		return
	}
	defer z.Close()
	data := new(Data)
	if err = json.NewDecoder(z).Decode(data); err != nil {
		c.data, c.err = nil, err
		return
	}
	if data.Error != "" {
		c.data, c.err = nil, errors.New(data.Error)
	}
	c.data, c.err, c.next = NewMarketData(data), nil, time.Now().Add(1*time.Minute)
}

func init() {
	market.Register("httpjson", func(fn string) (market.DataSource, error) {
		return NewClient(fn)
	})
}
