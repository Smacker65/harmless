package marketjson

import (
	et "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/market"
)

type Data struct {
	Error    string `json:",omitempty"`
	Stations []Station
}

type Station struct {
	Station string
	System  string
	Items   []Commodity
}

type Commodity struct {
	Name     string
	Category string
	Price    *market.PriceData
}

// NewData constructs JSON serializable Data from MarketData.
func NewData(md *market.Data) *Data {
	d := new(Data)
	for _, ms := range md.Stations {
		s := Station{
			Station: ms.Station.Name,
			System:  ms.System.Name,
			Items:   make([]Commodity, 0, len(ms.Commodities)),
		}
		for _, mc := range ms.Commodities {
			s.Items = append(s.Items, Commodity{
				Name:     mc.Name,
				Category: mc.Category,
				Price:    mc.PriceData,
			})
		}
		d.Stations = append(d.Stations, s)
	}
	return d
}

// NewMarketData reconstructs MarketData from JSON serializable Data.
func NewMarketData(d *Data) *market.Data {
	md := new(market.Data)
	cmap := make(map[string]*market.CommodityData)
	for _, s := range d.Stations {
		ms := &market.StationData{
			et.GetStarStation(s.System),
			make([]market.StationEntry, 0, len(s.Items)),
		}
		for _, i := range s.Items {
			k := i.Name + "\t" + i.Category
			mc, ok := cmap[k]
			if !ok {
				mc := &market.CommodityData{
					Commodity: market.Commodity{
						i.Name,
						i.Category,
					},
				}
				cmap[k] = mc
				md.Commodities = append(md.Commodities, mc)
			}
			ms.Commodities = append(ms.Commodities, market.StationEntry{mc, i.Price})
			mc.Stations = append(mc.Stations, market.CommodityEntry{ms, i.Price})
		}
		md.Stations = append(md.Stations, ms)
	}
	return md
}
