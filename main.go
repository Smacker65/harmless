package main

import (
	"bitbucket.org/tajtiattila/harmless/area"
	"bitbucket.org/tajtiattila/harmless/area/gen"
	"bitbucket.org/tajtiattila/harmless/db"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"bitbucket.org/tajtiattila/harmless/route"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/http/fcgi"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var (
	AreaStore   area.Store
	FontMap     *gen.FontMap
	MapTemplate *TmplDir
	StartTime   time.Time
)

const DefaultRadius = float64(50)

func main() {
	var addr, prefix, dataf, resf string
	flag.StringVar(&addr, "addr", ":6968", "listen address")
	flag.StringVar(&prefix, "prefix", "", "serve on this prefix")
	flag.StringVar(&dataf, "data", "./data", "path to data files")
	flag.StringVar(&resf, "res", "./res", "path to resource files")
	dofcgi := flag.Bool("fcgi", false, "fastcgi mode")
	flag.Parse()

	StartTime = time.Now()

	dbio, err := db.Open("dir", dataf)
	verify(err)
	defer dbio.Close()

	editlog, err := os.OpenFile(filepath.Join(dataf, "edit.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	verify(err)
	defer editlog.Close()

	AreaStore, err = area.OpenIO("sqlite", dbio)
	verify(err)

	verify(db.Load(dbio))

	verify(os.Chdir(resf))

	FontMap, err = gen.LoadDefaultFontMap("font")
	verify(err)

	AreaStore.SetMapper(gen.NewAreaMapper(db.DefaultData.Chart, area.XZPlane, DefaultRadius, FontMap))

	rand.Seed(time.Now().Unix())

	if prefix != "" && prefix[0] != '/' {
		prefix = "/" + prefix
	}
	if prefix == "/" {
		prefix = "" // root slash will be added in handlers
	}

	pd := &pagedata{
		Prefix: prefix,
		API:    prefix + "/api",
	}

	pages := []string{"map", "about", "setup", "edit"}

	MapTemplate = NewTmplDir("tmpl", pages...)
	MapTemplate.Funcs(MathFuncs)
	verify(MapTemplate.Load())

	for _, n := range pages {

		http.Handle(prefix+"/"+n+"/", func(page string) http.HandlerFunc {
			return func(w http.ResponseWriter, r *http.Request) {
				errh := Hlog(page, r).Enter()
				defer errh.Exit()
				errh.Err(MapTemplate.Update())
				errh.Err(MapTemplate.Lookup(page).Execute(w, pd))
			}
		}(n))
	}

	http.Handle(prefix+"/", http.StripPrefix(prefix+"/",
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "" {
				http.Redirect(w, r, "map/", http.StatusTemporaryRedirect)
			} else {
				http.FileServer(http.Dir("www")).ServeHTTP(w, r)
			}
		})))
	http.Handle(prefix+"/image/", http.StripPrefix(prefix+"/image/", http.FileServer(http.Dir("image"))))
	http.HandleFunc(prefix+"/api/search", searchfunc)
	http.HandleFunc(prefix+"/api/route", routeapifunc)
	http.HandleFunc(prefix+"/api/mapdata", mapdatafunc)
	http.HandleFunc(prefix+"/api/chart", ServeChart)
	http.Handle(prefix+"/api/edit", NewEditHandler(editlog))

	log.Println("Starting mapsrv on", addr+prefix)
	log.Println(len(db.DefaultData.Systems), "systems")
	if *dofcgi {
		l, err := net.Listen("tcp", addr)
		verify(err)
		verify(fcgi.Serve(l, http.DefaultServeMux))
	} else {
		verify(http.ListenAndServe(addr, nil))
	}
}

type pagedata struct {
	Prefix, API string
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func namelenf(s string) float64 {
	w := FontMap.Inhabited.TextWidth(s, 0.8)
	return w + 1
}

func searchfunc(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("searchfunc", req).Enter()
	defer errh.Exit()

	errh.Err(ServeJson(w, req,
		SystemSearch(
			req.FormValue("term"),
			db.IdSystem(req.FormValue("c")),
		)))
}

const jumperCookieName = "harmless.jumper"

func reqjumper(req *http.Request) (cj *route.SimpleJumper, qj *route.SimpleJumper) {
	errh := Hlog("reqjumper", req)
	defer errh.Exit()
	qj = parseJumper(req.FormValue("j"))
	for _, ck := range req.Cookies() {
		if ck.Name == jumperCookieName {
			cj = parseJumper(ck.Value)
			break
		}
	}
	if cj != nil {
		if qj == nil {
			qj = cj
		}
	} else {
		if qj == nil {
			qj = route.NewSimpleJumper(2.0, 0.8, 6.96) // basic sidey
		}
		cj = qj
	}
	return
}

func parseJumper(js string) *route.SimpleJumper {
	if js == "" {
		return nil
	}
	var err error
	jr := route.NewSimpleJumper(2.0, 0.8, 6.96) // basic sidey
	vs := strings.SplitN(js, "_", 6)
	if len(vs) > 5 {
		switch vs[5] {
		case "j":
			jr.ScoopFuel = false
			jr.Evaluator = route.OptTime
		case "s":
			jr.ScoopFuel = false
			jr.Evaluator = route.OptFuelUse
		case "x":
			jr.ScoopFuel = true
			jr.Evaluator = route.OptTime
		}
		vs = vs[:5]
	}
	v := make([]float64, 0, 5)
	for _, s := range vs {
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			break
		}
		v = append(v, f)
	}
	if err == nil {
		switch len(v) {
		case 5:
			jr.FuelUnits = int(v[4])
			fallthrough
		case 4:
			jr.FuelPower = v[3]
			fallthrough
		case 3:
			jr.FuelTank = v[2]
			fallthrough
		case 2:
			jr.MaxJumpFuel = v[1]
			fallthrough
		case 1:
			jr.MaxJumpDist = v[0]
		}
	}
	return jr
}
