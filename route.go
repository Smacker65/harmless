package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"bitbucket.org/tajtiattila/harmless/route"
	"fmt"
	"net/http"
)

func routeapifunc(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("routeapifunc", req).Enter()
	defer errh.Exit()

	s := db.IdSystem(req.FormValue("s"))
	d := db.IdSystem(req.FormValue("d"))
	var xr XRoute
	if s == nil || d == nil || s == d {
		xr.Error = fmt.Sprint("invalid start/dest", s, d)
	} else {
		_, jr := reqjumper(req)
		rr := route.NewAstarMap(db.DefaultData.Chart, jr).Route(s, d)
		if rr == nil {
			xr.Unreachable = true
		} else {
			rr.Visit(func(a, b *db.System, fulltank bool) {
				if fulltank && len(xr.Route) > 0 {
					xr.Route[len(xr.Route)-1].Refuel = true
				}
				xr.Route = append(xr.Route, XJump{System: b})
			})
		}
	}

	errh.Err(ServeJson(w, req, xr))
}

type XRoute struct {
	Route       []XJump `json:"route"`
	Unreachable bool    `json:"unreachable"`
	Error       string  `json:"error,omitempty"`
}

type XJump struct {
	Refuel bool `json:"refuel"`
	*db.System
}
