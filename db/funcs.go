package db

import (
	"bitbucket.org/tajtiattila/harmless/fuzzy"
	"sort"
	"strings"
)

var (
	DefaultData Data
	Systems     []*System
)

func Load(src IO) error {
	err := DefaultData.Load(src)
	Systems = DefaultData.Systems
	return err
}

func LoadDir(p string) error {
	src, err := Open("dir", p)
	if err != nil {
		return err
	}
	return Load(src)
}

// IdSystem finds returns the star by the id in DefaultData,
// or nil if it is not present
func IdSystem(id string) *System {
	return DefaultData.IdSystem(id)
}

func FindSystem(n string) (*System, error) {
	return DefaultData.FindSystem(n)
}

func MustFindSystem(n string) *System {
	return DefaultData.MustFindSystem(n)
}

func (d *Data) Load(src IO) error {
	return src.Read(d)
}

// IdSystem finds returns the star by the id, or nil if it is not present
func (d *Data) IdSystem(id string) *System {
	return d.SysIdMap[id]
}

// FindSystem finds a star based on untrusted (user) input
// using fuzzy search, or any error if can't decide which is the most likely
// match when there are multiple matching candidates.
func (d *Data) FindSystem(n string) (*System, error) {
	i, err := fuzzy.Find(n, starDict{d})
	if err != nil {
		return nil, err
	}
	return d.Systems[i], nil
}

func (d *Data) MustFindSystem(n string) *System {
	s, err := d.FindSystem(n)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *System) Sort() {
	sort.Sort(stnSort(s.Stations))
}

type stnSort []Station

func (s stnSort) Len() int      { return len(s) }
func (s stnSort) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s stnSort) Less(i, j int) bool {
	return strings.ToLower(s[i].Name) < strings.ToLower(s[j].Name)
}

func nameid(name string) string {
	var rv []rune
	uc := false
	for _, r := range name {
		var rx rune
		switch {
		case ('a' <= r && r <= 'z') || ('0' <= r && r <= '9'):
			rx = r
		case 'A' <= r && r <= 'Z':
			rx = r + 'a' - 'A'
		case r == '\'':
			// no op
		default:
			uc = true
		}
		if rx != 0 {
			if uc {
				rv, uc = append(rv, '_'), false
			}
			rv = append(rv, rx)
		}
	}
	return string(rv)
}

type starDict struct {
	*Data
}

func (d starDict) Len() int            { return len(d.Systems) }
func (d starDict) String(i int) string { return d.Systems[i].Name }
