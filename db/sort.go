package db

import (
	"sort"
)

func SortSystems(v []*System) {
	sort.Sort(sortsys(v))
	for _, sys := range v {
		SortStations(sys.Stations)
	}
}

type sortsys []*System

func (v sortsys) Len() int      { return len(v) }
func (v sortsys) Swap(i, j int) { v[i], v[j] = v[j], v[i] }

func (v sortsys) Less(i, j int) bool {
	in, jn := lc(v[i].Name), lc(v[j].Name)
	if in != jn {
		return in < jn
	}
	d := countRealStations(v[i]) - countRealStations(v[j])
	if d != 0 {
		return d > 0
	}
	for k := 0; k < 3; k++ {
		dc := v[i].P[k] - v[j].P[k]
		if dc != 0 {
			return dc < 0
		}
	}
	return false
}

func countRealStations(sys *System) int {
	n := 0
	for _, stn := range sys.Stations {
		if !stn.Inferred {
			n++
		}
	}
	return n
}

func SortStations(v []Station) {
	sort.Sort(sortstn(v))
}

type sortstn []Station

func (v sortstn) Len() int           { return len(v) }
func (v sortstn) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v sortstn) Less(i, j int) bool { return lc(v[i].Name) < lc(v[j].Name) }

func SortCategories(v []Category) {
	sort.Sort(sortcat(v))
	for _, cdt := range v {
		SortCommodities(cdt.Items)
	}
}

type sortcat []Category

func (v sortcat) Len() int           { return len(v) }
func (v sortcat) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v sortcat) Less(i, j int) bool { return lc(v[i].Name) < lc(v[j].Name) }

func SortCommodities(v []Commodity) {
	sort.Sort(sortcdt(v))
}

type sortcdt []Commodity

func (v sortcdt) Len() int           { return len(v) }
func (v sortcdt) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v sortcdt) Less(i, j int) bool { return lc(v[i].Name) < lc(v[j].Name) }

func SortShips(v []Ship) {
	sort.Sort(sortship(v))
}

type sortship []Ship

func (v sortship) Len() int           { return len(v) }
func (v sortship) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v sortship) Less(i, j int) bool { return lc(v[i].Name) < lc(v[j].Name) }

func SortComponents(v []Component) {
	sort.Sort(sortcomp(v))
}

type sortcomp []Component

func (v sortcomp) Len() int           { return len(v) }
func (v sortcomp) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v sortcomp) Less(i, j int) bool { return lc(v[i].Name) < lc(v[j].Name) }
