package db

import (
	"strings"
)

// Validity represents which version of the game the data
// was last checked in. Larger values mean "better"
type Validity int

const (
	Unknown Validity = 0

	Invalid Validity = 1 // "invalid" or any value other than one below

	Alpha4 Validity = 0xa4 // "Alpha4"

	PremiumBeta2 Validity = 0xb0 // "Premium Beta 2"

	Beta1 Validity = 0xb1 // "Beta1"
	Beta2 Validity = 0xb2 // "Beta2"
	Beta3 Validity = 0xb3 // "Beta3"

	Gamma Validity = 0xc0 // "Gamma"

	Current = Gamma
)

var (
	ValidityMap = map[Validity]string{
		Unknown: "",
		Invalid: "invalid",
		Alpha4:  "Alpha4",

		PremiumBeta2: "Premium Beta 2",

		Beta1: "Beta1",
		Beta2: "Beta2",
		Beta3: "Beta3",

		Gamma: "Gamma",
	}
	StrValidityMap = make(map[string]Validity)
)

func init() {
	for k, v := range ValidityMap {
		if k != Invalid && k != Unknown {
			StrValidityMap[strings.ToLower(v)] = k
		}
	}
}

// ParseValidity returns the integral validity for the input.
// It returns Unknown for an empty string, the value
// accorting to ValidityMap if found, or Invalid otherwise.
func ParseValidity(s string) Validity {
	if s == "" {
		return Unknown
	}
	if v, ok := StrValidityMap[strings.ToLower(s)]; ok {
		return v
	}
	return Invalid
}

func (v Validity) String() string {
	s, ok := ValidityMap[v]
	if !ok {
		s = ValidityMap[Invalid]
	}
	return s
}

type Security int

// Security represents the security of a system
const (
	SecUnknown Security = iota
	SecLow
	SecMedium
	SecHigh
)

func ParseSecurity(s string) Security {
	switch strings.ToLower(s) {
	case "low":
		return SecLow
	case "medium":
		return SecMedium
	case "high":
		return SecHigh
	}
	return SecUnknown
}

func (v Security) String() string {
	switch v {
	case SecLow:
		return "low"
	case SecMedium:
		return "medium"
	case SecHigh:
		return "high"
	}
	return "unknown"
}
