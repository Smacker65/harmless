package db

import (
	"fmt"
	"math"
	"strings"
)

// split system name into sector and label
func SplitLabel(name string) (sect, label string) {
	ll := strings.ToLower(name)
	if i := strings.Index(ll, " sector "); i != -1 {
		i += 7
		sect, label = name[:i], name[i+1:]
	} else {
		label = name
	}
	return
}

func Combine(old, newd *Data) *Data {
	data := &Data{
		DbStation: make(map[int64]*Station),
		Stations:  make(map[string]*Station),
	}
	data.Systems = append(data.Systems, old.Systems...)
	data.Cdt = append(data.Cdt, old.Cdt...)
	data.Ships = append(data.Ships, old.Ships...)
	data.Comps = append(data.Comps, old.Comps...)

	msysn, msysp := make(map[string]int), make(map[Vec3]int)
	for i, sys := range data.Systems {
		msysn[lc(sys.Name)] = i
		msysp[sys.P] = i
	}
	for _, nsys := range newd.Systems {
		in, nok := msysn[lc(nsys.Name)]
		ip, pok := msysp[nsys.P]
		var (
			osys *System
			cpst bool
		)
		switch {
		case nok && pok:
			osys = old.Systems[in]
			cpst = true
		case nok:
			osys = old.Systems[in]
			osys.P = nsys.P
			cpst = true
		case pok:
			osys = old.Systems[ip]
			osys.Name = nsys.Name
			cpst = true
		default:
			data.Systems = append(data.Systems, nsys)
		}
		if cpst {
			// TODO
		}
	}
	return data
}

func wipeIds(data *Data) {
	for _, sys := range data.Systems {
		sys.Id = ""
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			stn.Id = ""
		}
	}
}

const IdSeparator = '·'

func escn(s string) string {
	if strings.IndexRune(s, IdSeparator) == -1 {
		return s
	}
	r := make([]rune, 0, len(s))
	for _, ch := range s {
		if ch == IdSeparator {
			r = append(r, ch)
		}
		r = append(r, ch)
	}
	return string(r)
}

func (data *Data) AssumeValidity(v Validity) {
	if v == Unknown {
		return
	}
	for _, sys := range data.Systems {
		if sys.Valid == Unknown {
			sys.Valid = v
		}
		if sys.SystemDetail != nil && sys.SystemDetail.Valid == Unknown {
			sys.SystemDetail.Valid = v
		}
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			if stn.Valid == Unknown {
				stn.Valid = v
			}
		}
	}
}

// first find out which "system·station" and "station" values are unique
func uniqueIds(data *Data) map[string]int {
	muniq := make(map[string]int)
	for _, sys := range data.Systems {
		lsys := lc(escn(sys.Name))
		for _, stn := range sys.Stations {
			lstn := lc(escn(stn.Name))
			muniq[lsys+string(IdSeparator)+lstn]++
			muniq[lstn]++
		}
	}
	return muniq
}

// generate unique System.Id values
func (data *Data) CalcSystemIds() {
	data.SysIdMap = make(map[string]*System)
	dupes := make(map[string]int)
	for _, sys := range data.Systems {
		dupes[nameid(sys.Name)]++
	}
	for _, sys := range data.Systems {
		sid := nameid(sys.Name)
		if dupes[sid] < 2 {
			sys.Id = sid
		} else {
			sys.Id = Coordid(sys.P) + "__" + sid
		}
		data.SysIdMap[sys.Id] = sys
	}
}

func (data *Data) CalcSysStationIds() {
	muniq := uniqueIds(data)

	// generate station Ids
	data.Stations = make(map[string]*Station)
	for _, sys := range data.Systems {
		esys := escn(sys.Name)
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			estn := escn(stn.Name)
			if muniq[lc(estn)] < 2 {
				stn.Id = estn
			} else {
				esysstn := esys + string(IdSeparator) + estn
				if muniq[lc(esysstn)] < 2 {
					stn.Id = esysstn
				} else {
					stn.Id = Coordid(sys.P) + string(IdSeparator) + esysstn
				}
			}
		}
	}

	data.initStationMap(muniq)
}

// sort tables
func (data *Data) Sort() {
	SortSystems(data.Systems)
	SortCategories(data.Cdt)
	SortShips(data.Ships)
	SortComponents(data.Comps)
}

func (data *Data) InitStationMap() {
	data.initStationMap(uniqueIds(data))
}

func (data *Data) initStationMap(muniq map[string]int) {
	// fill in Stations map with unique Ids
	data.Stations = make(map[string]*Station)
	for _, sys := range data.Systems {
		lsys := lc(escn(sys.Name))
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			lstn := lc(escn(stn.Name))
			if muniq[lstn] < 2 {
				data.Stations[lstn] = stn
			}
			lsysstn := lsys + string(IdSeparator) + lstn
			if muniq[lsysstn] < 2 {
				data.Stations[lsysstn] = stn
			}
		}
	}

	// add predefined Ids
	for _, sys := range data.Systems {
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			if stn.Id != "" {
				data.Stations[lc(stn.Id)] = stn
			}
		}
	}
}

const (
	onePerLy = 32
	base32s  = "abcdefghijklmnopqrstuvwxyz234567"
)

func base32(value uint64, nbits int) string {
	const m = 5
	if nbits%m != 0 {
		nbits += m - nbits%m
	} else if nbits <= 0 {
		panic("base32 nbits invalid")
	}
	v := make([]byte, 0, nbits/m)
	for nshift := nbits / m; nshift > 0; nshift-- {
		shift := m * uint(nshift-1)
		chval := int((value >> (shift - 1)) & 0x1f)
		v = append(v, base32s[chval])
	}
	return string(v)
}

func Coordid(pos Vec3) string {
	w := int32(65536 * onePerLy)
	var ip, cp [3]int32
	for i := 0; i < 3; i++ {
		ip[i] = int32(math.Floor(pos[i]*onePerLy + 0.5))
	}
	var octpos uint64
	// 15 iterations will yield 131072/2**15 = 4.0 Ly precision
	// and everything will fit as long as the absolute value of
	// all coordinates are smaller than 65536.0, and it needs
	// only a 9-char (base32) value.
	const niter = 15
	for j := 0; j < niter; j++ {
		w /= 2
		for i := 0; i < 3; i++ {
			if cp[i] <= ip[i] {
				cp[i] += w
				octpos |= 1
			} else {
				cp[i] -= w
			}
			octpos <<= 1
		}
	}
	return base32(octpos, 3*niter)
}

func lc(s string) string {
	return strings.ToLower(s)
}

func (pos Vec3) String() string {
	return fmt.Sprintf("%v:%v:%v", pos[0], pos[1], pos[2])
}
