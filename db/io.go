package db

import (
	"encoding/json"
	"fmt"
	"os"
)

type IO interface {
	Read(*Data) error
	Write(*Data) error
	Close() error
}

type Driver interface {
	Open(spec string, flag int) (IO, error)
}

type DriverFunc func(spec string, flag int) (IO, error)

func (f DriverFunc) Open(spec string, flag int) (IO, error) {
	return f(spec, flag)
}

var drivers = make(map[string]Driver)

func RegisterDriver(name string, driver Driver) {
	if _, ok := drivers[name]; ok {
		panic("duplicate driver: " + name)
	}
	drivers[name] = driver
}

func Open(driver, path string) (IO, error) {
	return OpenIO(driver, path, os.O_RDONLY)
}

func OpenIO(driver, path string, flag int) (IO, error) {
	drv := drivers[driver]
	if drv == nil {
		return nil, fmt.Errorf(`db.Driver "%s" is not available`, driver)
	}
	return drv.Open(path, flag)
}

func (v Validity) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.String())
}

func (v *Validity) UnmarshalJSON(p []byte) error {
	var s string
	err := json.Unmarshal(p, &s)
	if err != nil {
		return err
	}
	*v = ParseValidity(s)
	return nil
}
