package db

import (
	"time"
)

type Data struct {
	Systems []*System   `json:"systems,omitempty"`
	Cdt     []Category  `json:"commodities,omitempty"` // commodity categories
	Ships   []Ship      `json:"ships,omitempty"`
	Comps   []Component `json:"components,omitempty"`

	SysIdMap map[string]*System  `json:"-"` // System.Id → *System
	Stations map[string]*Station `json:"-"` // Station.Id → *Station within Systems

	// The following fields are filled only if the Data is backed by Sqlite
	DbSystem  map[int64]*System  `json:"-"` // System.system_id → *System
	DbStation map[int64]*Station `json:"-"` // Station.station_id → *Station

	Chart `json:"-"` // octree chart for fast system lookup
}

type System struct {
	// unique ID from name, contains only lowercase latin letters, digits and underscores
	Id string `json:"id,omitempty"`

	Name string `json:"system"`
	P    Vec3   `json:"coords"`

	DbKey int64 `json:"-"`

	// Signif shows how important the system is.
	// Specifically, when displaying a bubble of systems with a certain radius,
	// the systems should be marked/labeled if Signif is greater than radius.
	// This condition should be true for approximately 10-20 evenly placed
	// systems for any bubble.
	Signif float64 `json:"significance,omitempty"`

	*SystemDetail `json:"detail,omitempty"`

	Stations []Station `json:"stations,omitempty"`

	Valid    Validity  `json:"valid"`
	Added    string    `json:"added,omitempty"`
	Modified time.Time `json:"modified"`
}

func (s *System) CanRefuel() bool { return len(s.Stations) != 0 }

type SystemDetail struct {
	Pop   int64    `json:"population"`
	Alleg string   `json:"allegiance,omitempty"`
	Econ  string   `json:"economy,omitempty"`
	Govt  string   `json:"government,omitempty"`
	Sec   Security `json:"security"`

	Valid Validity `json:"valid,omitempty"`
}

// Station within a System
//
// Canonical id to reference a station in vendor lists is
//    position '·' System.Name '·' Station.Name (always available)
//    System.Name '·' Station.Name (available when unique)
//    Station.Name (available when station name is unique)
// Neither of the fields may be empty, and if they happen to have a '·'
// in them, the slash is doubled, to remove ambiguity.
//
// '·' is the middle dot character (U+00B7)
//
// position should be one of the following formats:
//  x y z coordinates, integer part only (all values having with either '+', '-' or '±')
//  x y z coordinates, with at least 2 digfit fractional part (separated as above)
//  3-, 6- or 9-digit base32 octant position giving approximate position (down to a 4096, 128 or 4 Ly box)
type Station struct {
	Id   string  `json:"id,omitempty"`       // unique station id
	Name string  `json:"station"`            // station name
	Dist float64 `json:"distance,omitempty"` // distance from star in light seconds

	StationDetail

	Valid Validity `json:"valid"` // station is valid in this version

	System *System `json:"-"` // system this station is located in

	DbKey int64 `json:"-"` // sqlite db key
}

type StationDetail struct {
	// This flag means it is not a real station, but the system is known to have one.
	// An inferred station may appear only if there are no valid stations
	// with more recent validity. Therefore, if there are stations with "valid"="Beta2"
	// an inferred station with "valid"="Beta3" may still be present.
	Inferred bool `json:"inferred,omitempty"` // station is inferred based on system info

	Type string `json:"type,omitempty"` // outpost or station type

	Faction string   `json:"faction,omitempty"` // main faction
	Govt    string   `json:"government,omitempty"`
	Alleg   string   `json:"allegiance,omitempty"`
	Econ    []string `json:"economy,omitempty"`

	Shipyard *bool `json:"shipyard,omitempty"`           // has shipyard
	Outfit   *bool `json:"outfitting,omitempty"`         // has outfitting
	Cmarket  *bool `json:"commodities_market,omitempty"` // has commodity market
	Bmarket  *bool `json:"black_market,omitempty"`       // has black market
}

type Category struct {
	Name  string      `json:"category"`
	Items []Commodity `json:"items"`
}

type Commodity struct {
	Name     string   `json:"commodity"`
	AltNames []string `json:"altnames,omitempty"`
	Vendors  []string `json:"vendors,omitempty"`
}

type Ship struct {
	Name  string `json:"ship"`
	Manuf string `json:"manufacturer"`
	Mass  int    `json:"mass,omitempty"`

	Value     int `json:"value,omitempty"`
	Insurance int `json:"insurance,omitempty"`

	Outfit  []CompLoc `json:"outfit,omitempty"`
	Vendors []string  `json:"vendors,omitempty"`
}

type CompLoc struct {
	Type  string `json:"type"`
	Class int    `json:"clas"`
}

type Component struct {
	Name    string   `json:"component"`
	Vendors []string `json:"vendors,omitempty"`

	Type      string `json:"type"`
	Cost      int    `json:"cost,omitempty"`
	Insurance int    `json:"insurance,omitempty"`

	Class  string  `json:"class,omitempty"`
	Mass   float64 `json:"mass,omitempty"`
	Rating string  `json:"rating,omitempty"`
}
