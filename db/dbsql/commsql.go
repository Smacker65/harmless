package dbsql

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"database/sql"
)

type intpair struct {
	a, b int
}

func readComms(tx *sql.Tx, data *db.Data) {
	mc := make(map[int64]int)
	rows := query(tx, `SELECT category_id, name FROM Category;`)
	defer rows.Close()
	for rows.Next() {
		var (
			id  int64
			cat db.Category
		)
		perr(rows.Scan(&id, &cat.Name))

		mc[id], data.Cdt = len(data.Cdt), append(data.Cdt, cat)
	}
	perr(rows.Err())
	perr(rows.Close())

	mi := make(map[int64]intpair)
	rows = query(tx, `SELECT c.category_id, i.item_id, i.name
		FROM Item i INNER JOIN Category c ON i.category_id=c.category_id
			ORDER BY i.name;`)
	defer rows.Close()
	for rows.Next() {
		var (
			cid, id int64
			cdt     db.Commodity
		)
		perr(rows.Scan(&cid, &id, &cdt.Name))

		cat := &data.Cdt[mc[cid]]
		mi[id] = intpair{mc[cid], len(cat.Items)}
		cat.Items = append(cat.Items, cdt)
	}
	perr(rows.Err())
	perr(rows.Close())

	rows = query(tx, `SELECT item_id, alt_name FROM AltItemNames ORDER BY alt_name;`)
	for rows.Next() {
		var (
			id   int64
			altn string
		)
		perr(rows.Scan(&id, &altn))
		pair := mi[id]
		cdt := &data.Cdt[pair.a].Items[pair.b]
		cdt.AltNames = append(cdt.AltNames, altn)
	}
	perr(rows.Err())
	perr(rows.Close())
}

func writeComms(tx *sql.Tx, data *db.Data) {
	execs(tx, `DELETE FROM AltItemNames;`)
	execs(tx, `DELETE FROM Item;`)
	execs(tx, `DELETE FROM Category;`)

	mcat := newidadder(tx, `Category`)
	scdt := prep(tx, `INSERT INTO Item(category_id,name) VALUES(?,?);`)
	saltn := prep(tx, `INSERT INTO AltItemNames(item_id,alt_name) VALUES(?,?);`)

	for _, cat := range data.Cdt {
		icat := mcat.nameid(cat.Name)
		for _, cdt := range cat.Items {
			icdt := execn(scdt, icat, cdt.Name)
			for _, altn := range cdt.AltNames {
				exec(saltn, icdt, altn)
			}
		}
	}
}
