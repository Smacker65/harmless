package dbsql

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"database/sql"
	"fmt"
	"os"
)

type SqliteDB struct {
	*sql.DB
}

func NewSqliteDB(dbfn string) (*SqliteDB, error) {
	sdb, err := OpenDB(dbfn)
	if err != nil {
		return nil, err
	}
	return &SqliteDB{sdb}, nil
}

func (sdb *SqliteDB) Read(data *db.Data) error {
	tx, err := sdb.DB.Begin()
	if err != nil {
		return err
	}
	defer tx.Commit()

	return sdb.read(tx, data)
}

func (sdb *SqliteDB) read(tx *sql.Tx, data *db.Data) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	readSystems(tx, data)
	readShips(tx, data)
	readComps(tx, data)
	readComms(tx, data)

	data.Chart = db.NewOctreeChart(data.Systems)

	return
}

func (sdb *SqliteDB) Write(data *db.Data) (err error) {
	var tmpdata db.Data

	var tx *sql.Tx
	tx, err = sdb.DB.Begin()
	if err != nil {
		return
	}
	defer tx.Commit()

	err = sdb.read(tx, &tmpdata)
	if err == nil {
		data.CalcSystemIds()
		for _, sys := range data.Systems {
			if tsys, ok := tmpdata.SysIdMap[sys.Id]; ok {
				sys.DbKey = tsys.DbKey
			}
		}
	}

	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	if len(data.Systems) != 0 {
		writeSystems(tx, data)
	}
	if len(data.Ships) != 0 {
		writeShips(tx, data)
	}
	if len(data.Comps) != 0 {
		writeComps(tx, data)
	}
	if len(data.Cdt) != 0 {
		writeComms(tx, data)
	}

	return
}

func (sdb *SqliteDB) Close() error {
	return sdb.DB.Close()
}

func init() {
	db.RegisterDriver("sqlite", db.DriverFunc(func(fn string, flag int) (db.IO, error) {
		return NewSqliteDB(fn)
	}))
	db.RegisterDriver("dir", db.DriverFunc(func(path string, flag int) (db.IO, error) {
		jp, sp := path+"/elite.json", path+"/elite.sqlite"
		js, errj := os.Stat(jp)
		ss, errs := os.Stat(sp)
		impjson := errj == nil && (errs != nil || js.ModTime().After(ss.ModTime()))
		sio, err := NewSqliteDB(sp)
		if err != nil {
			return nil, err
		}
		if impjson {
			var data db.Data
			if err = db.NewJsonIO(jp).Read(&data); err != nil {
				fmt.Fprintln(os.Stdout, err)
			} else {
				if err = sio.Write(&data); err != nil {
					// can't write
					return db.NewJsonIO(jp), nil
				}
			}
		}
		return sio, err
	}))
}
