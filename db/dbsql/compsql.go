package dbsql

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"database/sql"
	"sort"
)

func readComps(tx *sql.Tx, data *db.Data) {
	m := make(map[int64]int)
	rows := query(tx, `SELECT c.name,t.name
			FROM Component c
			INNER JOIN Type t ON c.type_id=t.type_id
			ORDER BY c.name;`)
	defer rows.Close()
	for rows.Next() {
		var (
			id   int64
			comp db.Component
		)
		perr(rows.Scan(&id, &comp.Name, &comp.Type))

		m[id] = len(data.Comps)
		data.Comps = append(data.Comps, comp)
	}
	perr(rows.Err())
	perr(rows.Close())

	rows = query(tx, `SELECT component_id,station_id FROM ComponentVendor;`)
	for rows.Next() {
		var icomp, istn int64
		perr(rows.Scan(&icomp, &istn))
		comp := &data.Comps[m[icomp]]
		comp.Vendors = append(comp.Vendors, data.DbStation[istn].Id)
	}
	perr(rows.Err())
	perr(rows.Close())

	for i := range data.Comps {
		comp := &data.Comps[i]
		sort.Strings(comp.Vendors)
	}
}

func writeComps(tx *sql.Tx, data *db.Data) {
	execs(tx, `DELETE FROM Component;`)

	mtyp := newidadder(tx, `Type`)
	sc := prep(tx, `INSERT INTO Component(name,type_id) VALUES(?,?);`)
	scv := prep(tx, `INSERT INTO ComponentVendor(component_id,station_id) VALUES(?,?);`)

	for _, comp := range data.Comps {
		ityp := mtyp.nameid(comp.Type)
		icomp := execn(sc, comp.Name, ityp)
		for _, v := range comp.Vendors {
			if st, ok := data.Stations[v]; ok {
				exec(scv, icomp, st.DbKey)
			}
		}
	}
}
