package dbsql

import (
	"database/sql"
	"fmt"
	"strings"
)

type sqlStmt struct {
	sql string
	*sql.Stmt
}

type SqliteError struct {
	Sql  string
	Args []interface{}
	Err  error
}

func (e *SqliteError) Error() string {
	v := e.Sql + ": " + e.Err.Error()
	if e.Args != nil {
		v += "\nwith: " + fmt.Sprintln(e.Args)
	}
	return v
}

func query(tx *sql.Tx, qs string, args ...interface{}) *sql.Rows {
	rows, err := tx.Query(qs, args...)
	if err != nil {
		panic(&SqliteError{qs, args, err})
	}
	return rows
}

func prep(tx *sql.Tx, s string) *sqlStmt {
	stmt, err := tx.Prepare(s)
	if err != nil {
		panic(&SqliteError{s, nil, err})
	}
	return &sqlStmt{s, stmt}
}

func execs(tx *sql.Tx, s string, args ...interface{}) {
	_, err := tx.Exec(s, args...)
	if err != nil {
		panic(&SqliteError{s, args, err})
	}
}

func exec(s *sqlStmt, args ...interface{}) {
	_, err := s.Exec(args...)
	if err != nil {
		panic(&SqliteError{s.sql, args, err})
	}
}

func execn(s *sqlStmt, args ...interface{}) int64 {
	r, err := s.Exec(args...)
	if err != nil {
		panic(&SqliteError{s.sql, args, err})
	}
	n, err := r.LastInsertId()
	if err != nil {
		panic(&SqliteError{"@LASTINSERTID " + s.sql, args, err})
	}
	return n
}

type idadder struct {
	stmt *sqlStmt
	m    map[string]int64
}

func newidadder(tx *sql.Tx, tabname string) *idadder {
	a := &idadder{
		prep(tx, fmt.Sprintf(`INSERT INTO %s(name) VALUES(?);`, tabname)),
		make(map[string]int64),
	}
	rows := query(tx, fmt.Sprintf(`SELECT %s,name FROM %s;`, tabname+"_id", tabname))
	defer rows.Close()
	for rows.Next() {
		var (
			id   int64
			name string
		)
		perr(rows.Scan(&id, &name))
		a.m[lc(name)] = id
	}
	return a
}

func (a *idadder) nameid(name string) int64 {
	lname := lc(name)
	if n, ok := a.m[lname]; ok {
		return n
	}
	n := execn(a.stmt, name)
	a.m[lname] = n
	return n
}

func lc(s string) string {
	return strings.ToLower(s)
}

func perr(err error) {
	if err != nil {
		panic(err)
	}
}
