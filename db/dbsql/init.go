package dbsql

import (
	"database/sql"
	"github.com/mattn/go-sqlite3"
	"strings"
	"unicode"
)

func OpenDB(dbfn string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", dbfn)
	if err != nil {
		return nil, err
	}

	// foreign_keys must be enabled for each connection
	_, err = db.Exec("PRAGMA foreign_keys=on;")
	if err != nil {
		return nil, err
	}

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Commit()

	rows, err := tx.Query(`SELECT name FROM sqlite_master WHERE type='table';`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	tabs := make(map[string]bool)
	for rows.Next() {
		var name string
		if err = rows.Scan(&name); err != nil {
			return nil, err
		}
		tabs[strings.ToLower(name)] = true
	}
	rows.Close()

	// initialize missing tables
	for _, e := range vinitsql {
		if len(tabs) == 0 || (e.name != "" && !tabs[strings.ToLower(e.name)]) {
			_, err = tx.Exec(e.sql)
			if err != nil {
				return nil, err
			}
		}
	}

	return db, nil
}

var vinitsql []sqlinit

type sqlinit struct {
	name string
	sql  string
}

func init() {
	sdata := initsql

	_, ver, _ := sqlite3.Version()
	if ver < 3008002 {
		sdata = strings.Replace(sdata, "WITHOUT ROWID", "", -1)
	}

	stmts := strings.SplitAfter(sdata, ";\n")
	for i := range stmts {
		stmts[i] = strings.TrimSpace(stmts[i])
	}

	for _, stmt := range stmts {
		words := strings.FieldsFunc(stmt, func(r rune) bool {
			return unicode.IsSpace(r) || r == '('
		})
		if len(words) > 3 && words[0] == "CREATE" && words[1] == "TABLE" {
			vinitsql = append(vinitsql, sqlinit{words[2], stmt})
		} else {
			vinitsql = append(vinitsql, sqlinit{"", stmt})
		}
	}
}

var initsql = `

CREATE TABLE Added
 (
   added_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE(name)
 );

CREATE TABLE Valid
 (
   valid_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE(name)
 );

CREATE TABLE System
 (
   system_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   pos_x DOUBLE NOT NULL,
   pos_y DOUBLE NOT NULL,
   pos_z DOUBLE NOT NULL,
   significance DOUBLE NOT NULL,
   added_id INTEGER,
   modified DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,

   valid_id INTEGER NOT NULL,

   UNIQUE(pos_x, pos_y, pos_z),

   FOREIGN KEY (added_id) REFERENCES Added(added_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (valid_id) REFERENCES Valid(valid_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );

CREATE TABLE Allegiance
 (
   allegiance_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE(name)
 );

CREATE TABLE Government
 (
   government_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE(name)
 );

CREATE TABLE Economy
 (
   economy_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE(name)
 );

CREATE TABLE SystemDetail
 (
   system_id INTEGER NOT NULL,
   allegiance_id INTEGER NOT NULL,
   government_id INTEGER NOT NULL,
   economy_id INTEGER NOT NULL,
   population INTEGER NOT NULL,
   security INTEGER NOT NULL,

   valid_id INTEGER NOT NULL,

   UNIQUE (system_id),

   FOREIGN KEY (system_id) REFERENCES System(system_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (allegiance_id) REFERENCES Allegiance(allegiance_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (government_id) REFERENCES Government(government_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (economy_id) REFERENCES Economy(economy_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (valid_id) REFERENCES Valid(valid_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );

CREATE TABLE Station
 (
   station_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   system_id INTEGER NOT NULL,
   ls_from_star DOUBLE,

   -- json details
   detail BLOB,

   valid_id INTEGER NOT NULL,

   UNIQUE (system_id, name),

   FOREIGN KEY (system_id) REFERENCES System(system_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (valid_id) REFERENCES Valid(valid_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );


CREATE TABLE Ship
 (
   ship_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   manufacturer VARCHAR(40) COLLATE nocase,
   mass NUMBER NOT NULL DEFAULT 0,
   value INTEGER NOT NULL DEFAULT 0,

   UNIQUE (name)
 );


CREATE TABLE ShipVendor
 (
   ship_id INTEGER NOT NULL,
   station_id INTEGER NOT NULL,

   PRIMARY KEY (ship_id, station_id),

   FOREIGN KEY (ship_id) REFERENCES Ship(ship_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (station_id) REFERENCES Station(station_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 ) WITHOUT ROWID
;


CREATE TABLE Type
 (
   type_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE (name)
 );


CREATE TABLE Component
 (
   component_id INTEGER PRIMARY KEY,
   type_id INTEGER,
   name VARCHAR(40) COLLATE nocase,
   mass NUMBER NOT NULL,
   value INTEGER,

   UNIQUE (name)

   FOREIGN KEY (type_id) REFERENCES Type(type_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );


CREATE TABLE ComponentVendor
 (
   component_id INTEGER NOT NULL,
   station_id INTEGER NOT NULL,
   cost INTEGER,

   PRIMARY KEY (component_id, station_id),

   FOREIGN KEY (component_id) REFERENCES Component(component_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (station_id) REFERENCES Station(station_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 ) WITHOUT ROWID
;


CREATE TABLE Category
 (
   category_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE (name)
 );


CREATE TABLE Item
 (
   item_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   category_id INTEGER NOT NULL,

   UNIQUE (category_id, name),

   FOREIGN KEY (category_id) REFERENCES Category(category_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );


-- Some items have two versions of their name.
CREATE TABLE AltItemNames
 (
   alt_name VARCHAR(40) NOT NULL COLLATE nocase,
   item_id INTEGER NOT NULL,

   UNIQUE (alt_name),
   PRIMARY KEY (alt_name, item_id)
 );

CREATE TABLE AreaMap
 (
   system_id INTEGER NOT NULL,
   accessed DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
   hash VARCHAR(256) NOT NULL,
   map BLOB,

   FOREIGN KEY (system_id) REFERENCES System(system_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );

CREATE TABLE Price
 (
   item_id INTEGER NOT NULL,
   station_id INTEGER NOT NULL,
   ui_order INTEGER NOT NULL DEFAULT 0,
   -- how many credits will the station pay for this item?
   sell_to INTEGER NOT NULL,
   -- how many credits must you pay to buy at this station?
   buy_from INTEGER NOT NULL DEFAULT 0,
   modified DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
   demand INTEGER DEFAULT -1,
   demand_level INTEGER DEFAULT -1,
   stock INTEGER DEFAULT -1,
   stock_level INTEGER DEFAULT -1,

   PRIMARY KEY (item_id, station_id),

   FOREIGN KEY (item_id) REFERENCES Item(item_id)
   	ON UPDATE CASCADE
      ON DELETE CASCADE,
   FOREIGN KEY (station_id) REFERENCES Station(station_id)
   	ON UPDATE CASCADE
      ON DELETE CASCADE
 ) WITHOUT ROWID
;
`
