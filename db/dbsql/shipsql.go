package dbsql

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"database/sql"
	"sort"
)

func readShips(tx *sql.Tx, data *db.Data) {
	m := make(map[int64]int)
	rows := query(tx, `SELECT ship_id,name,value FROM Ship ORDER BY name;`)
	defer rows.Close()
	for rows.Next() {
		var (
			id   int64
			ship db.Ship
		)
		perr(rows.Scan(&id, &ship.Name, &ship.Value))

		m[id] = len(data.Ships)
		data.Ships = append(data.Ships, ship)
	}
	perr(rows.Err())
	perr(rows.Close())

	tru := true

	rows = query(tx, `SELECT ship_id,station_id FROM ShipVendor;`)
	for rows.Next() {
		var iship, istn int64
		perr(rows.Scan(&iship, &istn))
		ship := &data.Ships[m[iship]]
		ship.Vendors = append(ship.Vendors, data.DbStation[istn].Id)
		data.DbStation[istn].Shipyard = &tru
	}
	perr(rows.Err())
	perr(rows.Close())

	for i := range data.Ships {
		ship := &data.Ships[i]
		sort.Strings(ship.Vendors)
	}
}

func writeShips(tx *sql.Tx, data *db.Data) {
	execs(tx, `DELETE FROM ShipVendor;`)
	execs(tx, `DELETE FROM Ship;`)

	ss := prep(tx, `INSERT INTO Ship(name,value) VALUES(?,?);`)
	ssv := prep(tx, `INSERT INTO ShipVendor(ship_id,station_id) VALUES(?,?);`)

	for _, ship := range data.Ships {
		iship := execn(ss, ship.Name, ship.Value)
		for _, v := range ship.Vendors {
			if st, ok := data.Stations[v]; ok {
				exec(ssv, iship, st.DbKey)
			}
		}
	}
}
