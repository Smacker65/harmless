package dbsql

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"database/sql"
	"encoding/json"
)

func readSystems(tx *sql.Tx, data *db.Data) {
	data.Stations = make(map[string]*db.Station)
	m := make(map[int64]*db.System)
	rows := query(tx, `SELECT
		sys.system_id, sys.name, sys.pos_x, sys.pos_y, sys.pos_z, sys.significance,
		added.name, sys.modified, sys.valid_id
		FROM System sys INNER JOIN added ON sys.added_id=added.added_id
			ORDER BY sys.name;`)
	defer rows.Close()
	for rows.Next() {
		sys := new(db.System)
		perr(rows.Scan(&sys.DbKey, &sys.Name, &sys.P[0], &sys.P[1], &sys.P[2], &sys.Signif, &sys.Added, &sys.Modified, &sys.Valid))

		sys.Modified = sys.Modified.UTC()

		m[sys.DbKey], data.Systems = sys, append(data.Systems, sys)
	}
	perr(rows.Err())
	perr(rows.Close())

	rows = query(tx, `SELECT
		sd.system_id, sd.population, allegiance.name, government.name, economy.name, sd.security, sd.valid_id
		FROM SystemDetail sd
			INNER JOIN allegiance ON sd.allegiance_id=allegiance.allegiance_id
			INNER JOIN economy ON sd.economy_id=economy.economy_id
			INNER JOIN government ON sd.government_id=government.government_id;`)
	defer rows.Close()
	for rows.Next() {
		var (
			id               int64
			pop              int64
			alleg, eco, govt string
			sec              db.Security
			valid            db.Validity
		)
		perr(rows.Scan(&id, &pop, &alleg, &govt, &eco, &sec, &valid))
		if sys, ok := m[id]; ok {
			sys.SystemDetail = &db.SystemDetail{
				Pop:   pop,
				Alleg: alleg,
				Econ:  eco,
				Govt:  govt,
				Sec:   sec,
				Valid: valid,
			}
		}
	}
	perr(rows.Err())
	perr(rows.Close())

	rows = query(tx, `SELECT
		system_id, station_id, name, ls_from_star, detail, valid_id
		FROM Station ORDER BY name;`)
	defer rows.Close()
	for rows.Next() {
		var (
			sysid, stid int64
			stn         db.Station
			detail      []byte
		)
		perr(rows.Scan(&sysid, &stid, &stn.Name, &stn.Dist, &detail, &stn.Valid))
		if sys, ok := m[sysid]; ok {

			stn.DbKey = stid

			perr(json.Unmarshal(detail, &stn.StationDetail))

			sys.Stations = append(sys.Stations, stn)
		}
	}

	data.CalcSystemIds()
	data.CalcSysStationIds()
	updateRefs(data)

	return
}

func writeSystems(tx *sql.Tx, data *db.Data) {
	execs(tx, `DELETE FROM SystemDetail;`)
	execs(tx, `DELETE FROM Station;`)
	execs(tx, `DELETE FROM AreaMap;`)

	oldsysm := make(map[int64]bool)
	rows := query(tx, `SELECT system_id FROM System;`)
	defer rows.Close()
	for rows.Next() {
		var id int64
		perr(rows.Scan(&id))
		oldsysm[id] = true
	}
	perr(rows.Err())
	perr(rows.Close())

	madd := newidadder(tx, `Added`)
	ssysins := prep(tx, `INSERT INTO System
		(name,pos_x,pos_y,pos_z,significance,added_id,modified,valid_id)
		VALUES(?,?,?,?,?,?,?,?);`)
	ssysupd := prep(tx, `UPDATE System SET
		name=?, pos_x=?, pos_y=?, pos_z=?, significance=?, added_id=?, modified=?, valid_id=?
		WHERE system_id=?;`)
	ssysdel := prep(tx, `DELETE FROM System WHERE system_id=?;`)
	mgovt := newidadder(tx, `Government`)
	mecon := newidadder(tx, `Economy`)
	malleg := newidadder(tx, `Allegiance`)
	ssysd := prep(tx, `INSERT INTO SystemDetail
		(system_id,allegiance_id,government_id,economy_id,population,security,valid_id) VALUES(?,?,?,?,?,?,?)`)
	sstn := prep(tx, `INSERT INTO Station
		(system_id, name, ls_from_star, detail, valid_id)
		VALUES(?,?,?,?,?);`)

	sv := prep(tx, `INSERT OR IGNORE INTO Valid(valid_id,name) VALUES(?,?);`)
	for k, v := range db.ValidityMap {
		exec(sv, int(k), v)
	}

	for _, sys := range data.Systems {
		iadd := madd.nameid(sys.Added)
		va := []interface{}{sys.Name, sys.P[0], sys.P[1], sys.P[2], sys.Signif, iadd, sys.Modified, sys.Valid}
		if sys.DbKey != 0 {
			exec(ssysupd, append(va, sys.DbKey)...)
			delete(oldsysm, sys.DbKey)
		} else {
			sys.DbKey = execn(ssysins, va...)
		}
		sd := sys.SystemDetail
		if sd != nil {
			igovt := mgovt.nameid(sd.Govt)
			iecon := mecon.nameid(sd.Econ)
			ialleg := malleg.nameid(sd.Alleg)
			exec(ssysd, sys.DbKey, ialleg, igovt, iecon, sd.Pop, sd.Sec, sd.Valid)
		}
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			detail, err := json.Marshal(stn.StationDetail)
			perr(err)
			istn := execn(sstn, sys.DbKey, stn.Name, stn.Dist, detail, stn.Valid)
			stn.DbKey = istn
		}
	}

	for oldid := range oldsysm {
		exec(ssysdel, oldid)
	}

	data.CalcSystemIds()
	updateRefs(data)
}

func updateRefs(data *db.Data) {
	data.DbSystem = make(map[int64]*db.System)
	data.DbStation = make(map[int64]*db.Station)
	for _, sys := range data.Systems {
		data.DbSystem[sys.DbKey] = sys
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			data.DbStation[stn.DbKey] = stn
			stn.System = sys
		}
	}
}
