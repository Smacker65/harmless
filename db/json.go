package db

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type JsonIO struct {
	fn string
}

func init() {
	RegisterDriver("json", DriverFunc(func(fn string, flag int) (IO, error) {
		return NewJsonIO(fn), nil
	}))
}

func NewJsonIO(fn string) *JsonIO {
	return &JsonIO{fn}
}

func (js *JsonIO) Read(data *Data) error {
	f, err := os.Open(js.fn)
	if err != nil {
		return err
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(data)

	data.CalcSystemIds()
	data.InitStationMap()
	data.Chart = NewOctreeChart(data.Systems)

	return err
}

func (js *JsonIO) Write(data *Data) error {
	wipeIds(data)

	data.Sort()

	p, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		return err
	}

	data.CalcSystemIds()

	return ioutil.WriteFile(js.fn, p, 644)
}

func (js *JsonIO) Close() error {
	return nil
}

func (u Vec2) MarshalJSON() ([]byte, error) {
	return json.Marshal(xyVec{u[0], u[1]})
}

func (u *Vec2) UnmarshalJSON(p []byte) (err error) {
	var xy xyVec
	if err = json.Unmarshal(p, &xy); err == nil {
		(*u)[0], (*u)[1] = xy.X, xy.Y
	}
	return
}

type xyVec struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

func (v Security) MarshalJSON() ([]byte, error) {
	if v == SecUnknown {
		return []byte(`null`), nil
	}
	return []byte(`"` + v.String() + `"`), nil
}

func (v *Security) UnmarshalJSON(p []byte) (err error) {
	var s string
	if err = json.Unmarshal(p, &s); err == nil {
		*v = ParseSecurity(s)
	}
	return
}
