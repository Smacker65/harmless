package db

import (
	"fmt"
	"math"
	"math/rand"
	"testing"
)

func starv(n int, centerofs Vec3, scale float64) []*System {
	v := make([]*System, n)
	for i := range v {
		p := Vec3{rand.Float64(), rand.Float64(), rand.Float64()}
		for k := 0; k < 3; k++ {
			p[k] = centerofs[k] + (p[k]-0.5)*scale
		}
		n := fmt.Sprint("System", i)
		v[i] = &System{Id: n, Name: n, P: p}
	}
	return v
}

type t8 struct {
	v []*System
	t *OctreeChart
}

func (t8 *t8) test(t *testing.T, c Vec3, r float64) {
	rounderr := r / math.Pow(2, 50) // float64 has 52 bits mantissa
	var lo, hi, oct int
	for _, s := range t8.v {
		d := c.Sub(s.P).Abs()
		if d < r+rounderr {
			hi++
		}
		if d <= r {
			lo++
		}
	}
	t8.t.Sphere(c, r, func(*System, float64) { oct++ })
	if lo <= oct && oct <= hi {
		if testing.Verbose() && lo != hi {
			t.Logf("octtree sphere ok: lo=%d oct=%d hi=%d", lo, oct, hi)
		}
		return
	}
	t.Errorf("octtree sphere failed: lo=%d oct=%d hi=%d", lo, oct, hi)
}

func TestOctreeChart(t *testing.T) {
	count := 100
	if testing.Short() {
		count = 5
	}
	for i := 0; i != count; i++ {
		center, radius := Vec3{100, 50, 20}, float64(80)
		sv := starv(10000, center, radius)
		t8 := &t8{sv, NewOctreeChart(sv)}

		for j := 0; j != count; j++ {
			var c Vec3
			for k := 0; k < 3; k++ {
				c[k] = center[k] + radius*(rand.Float64()*4-2)
			}
			r := rand.Float64() * 2 * radius
			t8.test(t, c, r)
			r = rand.Float64() * 0.1 * radius
			t8.test(t, c, r)
		}

		p := rand.Perm(len(sv))
		pa, pb := p[:count], p[count:]
		for i := range pa {
			a, b := sv[pa[i]], sv[pb[i]]
			r := a.P.Sub(b.P).Abs()
			t8.test(t, a.P, r)
			t8.test(t, b.P, r)
		}
	}
}
