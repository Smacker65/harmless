package testdata

import (
	. "bitbucket.org/tajtiattila/harmless/db"
	"fmt"
	"strconv"
)

type stardata struct {
	data    Data
	dupname map[string]*System
	duppos  map[string]*System
}

func makestardata() stardata {
	return stardata{
		dupname: make(map[string]*System),
		duppos:  make(map[string]*System),
	}
}

type xsys struct {
	*System
}

func (d *stardata) sys(name string, x, y, z float64) *xsys {
	i := len(d.data.Systems)
	d.data.Systems = append(d.data.Systems, System{Name: name, P: Vec3{x, y, z}})
	s := &d.data.Systems[i]

	p := starcoordstr(s.P, 32)
	in, ic := starid(s)
	if d.duppos[p] != nil {
		logf("duplicate system at the same position %s: '%s' and '%s'", p, d.duppos[p].Name, s.Name)
	}
	d.duppos[p] = s

	if d.dupname[s.Name] == nil {
		s.Id = in
	} else {
		logf("duplicate system with the same name: %s", s.Name)
		s.Id = in + "_" + ic
	}
	d.dupname[s.Name] = s

	return &xsys{s}
}

func (sys *xsys) st(names ...string) {
	for _, name := range names {
		sys.Stations = append(sys.Stations, Station{System: sys.System, Name: name})
	}
}

func init() {
	d := makestardata()

	d.sys("LDS 1503A", -43.468750, 16.437500, -26.468750)
	d.sys("LHS 5072", -33.343750, 15.156250, -27.531250).st("Dutton Beacon")
	d.sys("LHS 140", -34.531250, 16.187500, -23.062500).st("*LHS 140")
	d.sys("Exbeur", -35.156250, 27.843750, -33.656250).st("Bush High")
	d.sys("23 H. Camelopardalis", -37.000000, 26.250000, -36.625000)
	d.sys("WREGOE WV-E D11-104", -45.437500, 31.312500, -25.125000)
	d.sys("LP 7-226", -42.781250, 31.750000, -27.250000).st("Christopher Colony")
	d.sys("Boro Kacharis", -27.125000, 18.906250, -25.875000).st("Strekalov Terminal")
	d.sys("LP 5-88", -27.281250, 19.593750, -23.031250).st("Gurragchaa Station")
	d.sys("LHS 2088", -23.312500, 25.718750, -28.343750)
	d.sys("LHS 256", -18.000000, 21.437500, -24.062500)
	d.sys("LHS 2126", -19.593750, 22.250000, -22.968750)
	d.sys("LFT 671", -17.875000, 24.093750, -22.718750)
	d.sys("LFT 668", -19.031250, 25.656250, -24.218750)
	d.sys("LHS 246", -19.000000, 22.562500, -28.218750).st("Kurland Dock")
	d.sys("LHS 250", -19.187500, 24.343750, -29.531250).st("Zamka Dock")
	d.sys("LFT 679", -29.812500, 31.531250, -30.218750)
	d.sys("LHS 283", -20.781250, 29.593750, -23.531250).st("Renenbellot Oasis")
	d.sys("Ehlangai", -11.218750, 32.531250, -23.031250).st("Revin Laboratory")
	d.sys("Youdu", -7.843750, 33.875000, -26.125000)
	d.sys("LHS 2191", -13.031250, 28.312500, -25.500000)
	d.sys("36 Ursae Majoris", -11.125000, 32.937500, -23.000000).st("Richards Station")
	d.sys("PI-1 Ursae Minoris", -52.187500, 39.750000, -24.968750)
	d.sys("Aeolus", -43.281250, 37.281250, -31.531250).st("*Aeolus")
	d.sys("Zhu Rong", -39.875000, 34.937500, -24.187500).st("Stuart Ring")
	d.sys("LHS 2610", -41.312500, 40.437500, -27.375000)
	d.sys("Quiness", -46.468750, 37.781250, -24.937500).st("Lockyer Rest")
	d.sys("LFT 1072", -45.906250, 43.718750, -23.250000).st("Nansen Inheritance")
	d.sys("LP 62-147", -20.500000, 38.500000, -30.312500)
	d.sys("Ackycha", -29.031250, 39.812500, -25.031250).st("Hurston Base")
	d.sys("LP 37-75", -27.250000, 38.500000, -33.656250).st("*LP 37-75")
	d.sys("Huokang", -12.187500, 35.468750, -25.281250).st("Darwin Terminal")
	d.sys("LHS 2522", -24.812500, 56.531250, -23.437500).st("Weyl Estate")
	d.sys("LFT 747", -15.187500, 43.187500, -26.906250)
	d.sys("LHS 494", -50.562500, 11.468750, -6.187500)
	d.sys("Hemsut", -53.875000, 16.687500, -10.125000)
	d.sys("Yinfu", -39.937500, 16.187500, -10.687500)
	d.sys("Alderamin", -47.500000, 7.843750, -9.468750).st("Sullivan Port")
	d.sys("Eta Cephei", -45.125000, 9.375000, -6.437500).st("Isherwood Ring")
	d.sys("LHS 3461", -31.343750, 14.718750, -9.718750)
	d.sys("Alrai", -38.718750, 12.312500, -21.625000).st("Resnik Dock")
	d.sys("Perendi", -52.562500, 33.093750, -11.875000).st("Guidoni Landing")
	d.sys("LP 71-157", -52.343750, 28.375000, -8.031250).st("Linenger Hub")
	d.sys("LP 25-2", -48.937500, 27.250000, -19.562500).st("Fourneyron Station")
	d.sys("LFT 1446", -38.812500, 18.250000, -7.937500).st("Ray Colony")
	d.sys("Ross 1057", -32.312500, 26.187500, -12.437500).st("Wang Estate")
	d.sys("Tun", -47.968750, 20.968750, -17.531250).st("Tun Wart")
	d.sys("LFT 1073", -38.937500, 32.562500, -21.656250).st("Roberts Camp")
	d.sys("LHS 3297", -36.468750, 22.687500, -16.531250)
	d.sys("Naraka", -34.093750, 26.218750, -5.531250).st("Novitski Oasis")
	d.sys("LP 9-231", -42.156250, 25.531250, -19.593750)
	d.sys("Ross 1069", -50.531250, 10.625000, 1.031250)
	d.sys("Eos", -51.875000, 16.781250, -0.375000).st("*Eos")
	d.sys("LFT 1487", -55.000000, 15.156250, 4.437500)
	d.sys("Wolf 1409", -56.125000, 29.812500, 9.062500).st("Al-Khalili Refinery")
	d.sys("BY Draconis", -48.312500, 21.187500, 7.937500)
	d.sys("G 205-38", -51.656250, 19.125000, 11.406250)
	d.sys("Pi-fang", -34.656250, 22.843750, -4.593750).st("Brooks Estate")
	d.sys("26 Draconis", -39.000000, 24.906250, -0.656250).st("*26 Draconis")
	d.sys("LFT 1361", -38.781250, 24.718750, -0.500000)
	d.sys("LP 103-294", -37.187500, 19.000000, -0.187500)
	d.sys("h Draconis", -39.843750, 29.562500, -3.906250).st("Brislington")
	d.sys("CM Draco", -35.687500, 30.937500, 2.156250).st("Anderson Escape")
	d.sys("LFT 1421", -45.468750, 18.562500, 12.593750).st("Volkov Plant")
	d.sys("NLTT 46621", -43.812500, 20.343750, 4.812500).st("Bradley Inheritance")
	d.sys("LHS 3343", -38.937500, 21.562500, 11.437500)
	d.sys("LHS 446", -44.312500, 31.625000, 11.156250)
	d.sys("Paul-Friedrichs Star", -48.156250, 34.312500, 8.781250).st("Paul-Friedrichs Expedition Base Camp")
	d.sys("LHS 215", -20.593750, 13.281250, -18.312500).st("*LHS 215")
	d.sys("44 chi Draconis", -22.562500, 12.375000, -5.406250).st("Ferguson Platform", "Adams Dock")
	d.sys("DX 799", -22.968750, 12.312500, -17.906250)
	d.sys("Elphin", -30.125000, 8.187500, -17.000000)
	d.sys("LHS 2459", -11.000000, 10.781250, -8.218750)
	d.sys("DP Draconis", -17.500000, 25.968750, -11.375000)
	d.sys("Tollan", -14.781250, 20.000000, -21.968750).st("*Tollan")
	d.sys("LP 64-194", -21.656250, 32.218750, -16.218750).st("Longyear Survey")
	d.sys("Nang Ta-khian", -18.218750, 26.562500, -6.343750).st("Hadwell Orbital", "Hay Point")
	d.sys("G 239-25", -22.687500, 25.812500, -6.687500).st("Bresnik Mine")
	d.sys("LFT 880", -22.812500, 31.406250, -18.343750).st("Baker Platform")
	d.sys("LHS 2211", -12.531250, 24.281250, -21.000000)
	d.sys("Sz Ursae Majoris", -13.062500, 21.968750, -13.875000)
	d.sys("LHS 293", 1.843750, 28.312500, -14.437500).st("Onufriyenko Retreat")
	d.sys("LHS 287", 1.031250, 29.593750, -16.500000).st("Cox Ring")
	d.sys("LHS 316", -5.875000, 24.406250, -9.343750)
	d.sys("Ross 1003", -4.312500, 33.906250, -11.968750).st("Vaugh Dock")
	d.sys("Chara", -4.843750, 26.656250, -4.781250).st("Schade Dock")
	d.sys("Gweir", 3.125000, 31.750000, -11.625000)
	d.sys("Beurex", -1.656250, 28.531250, -7.843750)
	d.sys("Ross 905", 4.468750, 31.937500, -7.218750).st("Mitchell Refinery")
	d.sys("Xi Ursae Majoris", 2.656250, 27.093750, -9.937500).st("Comino Dock")
	d.sys("61 Ursae Majoris", 0.562500, 30.093750, -8.687500).st("Murray Claim")
	d.sys("G 148-13", -0.406250, 27.093750, -7.062500)
	d.sys("LHS 3549", -29.625000, 6.125000, -1.937500).st("Conrad Colony")
	d.sys("LHS 3558", -24.812500, 6.781250, -4.437500)
	d.sys("LHS 465", -23.937500, 12.218750, -0.625000)
	d.sys("LHS 455", -16.906250, 10.218750, -3.437500)
	d.sys("LP 71-165", -20.906250, 11.093750, -2.218750)
	d.sys("G 202-48", -15.531250, 14.375000, 1.875000)
	d.sys("Austern", -25.156250, 15.343750, 9.375000)
	d.sys("LP 229-17", -23.031250, 9.031250, 8.968750)
	d.sys("G 203-51", -15.875000, 12.031250, 5.343750)
	d.sys("G 203-47", -18.343750, 14.250000, 7.093750).st("Hopkins Settlement", "Zhen Outpost", "Horowitz City")
	d.sys("WISE 1405+5534", -8.031250, 13.437500, -1.843750)
	d.sys("Flousop", -1.625000, 16.812500, 4.000000).st("Mcculley Refinery")
	d.sys("Bidmere", -7.437500, 13.218750, 1.906250)
	d.sys("Lalande 29917", -26.531250, 22.156250, -4.562500).st("Hopkins Hanger")
	d.sys("DN Draconis", -27.093750, 21.625000, 0.781250)
	d.sys("WISE 1647+5632", -21.593750, 17.718750, 1.750000).st("Hirase Platform")
	d.sys("Acihaut", -18.500000, 25.281250, -4.000000).st("Mastracchio Base", "Cuffey Plant")
	d.sys("LHS 3006", -21.968750, 29.093750, -1.718750).st("WCM Transfer Orbital")
	d.sys("BD+47 2112", -14.781250, 33.468750, -0.406250).st("Olivas Settlement")
	d.sys("Hermitage", -28.750000, 25.000000, 10.437500)
	d.sys("LHS 3262", -24.125000, 18.843750, 4.906250).st("Lacaille Prospect", "Sy Base")
	d.sys("Tilian", -21.531250, 22.312500, 10.125000).st("Maunder's Hope", "Keeler Rest")
	d.sys("LHS 417", -18.312500, 18.187500, 4.906250).st("Gernhardt Camp")
	d.sys("Rakapila", -14.906250, 33.625000, 9.125000).st("Stone Enterprise")
	d.sys("Aulin", -19.687500, 32.687500, 4.750000).st("Onufrienko Station", "Aulin Enterprise", "Harbaugh Station")
	d.sys("Keries", -18.906250, 27.218750, 12.593750).st("Derrickson's Escape")
	d.sys("LP 378-541", 1.187500, 20.718750, 2.343750)
	d.sys("DG Canum Venaticorum", -3.125000, 25.531250, 2.687500)
	d.sys("Wyrd", -11.625000, 31.531250, -3.937500).st("Vonarburg Co-operative")
	d.sys("Ross 1015", -6.093750, 29.468750, 3.031250).st("Bowersox Mines")
	d.sys("Beta Comae Berenices", -1.687500, 29.656250, 2.031250).st("Landsteiner Plant")
	d.sys("2Mass 1503+2525", -6.187500, 18.000000, 8.125000)
	d.sys("37 Xi Bootis", -4.218750, 19.093750, 9.812500).st("Leeuwenhoek Works")
	d.sys("LP 271-25", -10.468750, 31.843750, 7.312500)
	d.sys("LHS 2887", -7.343750, 26.781250, 5.718750).st("Massimino Dock")
	d.sys("SDSS J1416+1348", -0.687500, 27.156250, 12.000000)
	d.sys("Malina", -48.187500, 23.000000, 14.406250).st("Humboldt Prospect")
	d.sys("LHS 6309", -33.562500, 33.125000, 13.468750).st("Rangarajan Platform")
	d.sys("LHS 3281", -30.812500, 22.500000, 13.250000)
	d.sys("Culann", -36.062500, 14.625000, 13.812500)
	d.sys("72 Herculis", -32.875000, 24.687500, 22.218750).st("Jernigan Landing")
	d.sys("LHS 457", -31.750000, 16.656250, 15.500000)
	d.sys("Caer Bran", -31.281250, 14.312500, 13.781250).st("Browns Port")
	d.sys("Marcov's Point", -22.343750, 14.625000, 17.656250)
	d.sys("Connla", -29.906250, 14.562500, 13.656250).st("Nelson Port")
	d.sys("86 Mu Herculis", -19.500000, 11.531250, 14.875000).st("Mcmonagle Hub")
	d.sys("LP 275-68", -23.343750, 25.062500, 15.187500)
	d.sys("Ross 868", -24.687500, 19.125000, 21.312500)
	d.sys("Wolf 654", -28.468750, 22.406250, 14.812500).st("Diesel Survey")
	d.sys("Ross 860", -20.125000, 19.281250, 19.000000).st("Lockhart Enterprise")
	d.sys("Zeta Herculis", -21.343750, 22.375000, 16.250000).st("Jemison Plant")
	d.sys("LHS 411", -13.125000, 23.593750, 18.625000).st("Schottky Port")
	d.sys("G 181-6", -20.312500, 20.312500, 14.062500)
	d.sys("Miquich", -17.718750, 19.125000, 19.593750).st("Onufrienko Hanger")
	d.sys("Hepa", -29.437500, 34.468750, 21.531250).st("Ryman Dock")
	d.sys("LP 386-49", -25.531250, 33.375000, 26.000000)
	d.sys("OT Serpentis", -11.125000, 30.343750, 18.406250)
	d.sys("Veren's stop", -12.968750, 21.968750, 20.281250)
	d.sys("CE Bootis", -5.406250, 29.375000, 16.468750).st("Moseley Park")
	d.sys("41 Gamma Serpentis", -12.125000, 26.000000, 22.875000).st("Speke Dock")
	d.sys("Arcturus", -3.531250, 34.156250, 12.968750).st("Fort Harrison")
	d.sys("DE Bootis", -7.437500, 32.625000, 17.000000)
	d.sys("Ross 52", -8.437500, 29.156250, 13.312500)
	d.sys("LHS 396", -9.875000, 30.843750, 20.468750)
	d.sys("LHS 3057", -50.750000, 50.031250, -13.281250).st("Al-Khayyam Colony")
	d.sys("Manamaya", -43.531250, 35.031250, -19.156250).st("*Manamaya")
	d.sys("Ross 1051", -37.218750, 44.500000, -5.062500).st("Abetti Platform")
	d.sys("Thaumas", -50.437500, 62.718750, -6.687500)
	d.sys("WREDGUIA LW-E D11-125", -50.406250, 54.593750, -11.093750)
	d.sys("Wolf 483", -32.687500, 60.812500, -13.937500)
	d.sys("WREGOE SB-N B48-1", -47.031250, 57.593750, -8.562500)
	d.sys("Hera", -36.812500, 55.593750, -17.968750).st("Moore Station")
	d.sys("LP 102-320", -60.625000, 35.125000, 3.218750).st("Springer Port")
	d.sys("ADS 10329", -61.531250, 45.781250, 1.500000)
	d.sys("Theta Draconis", -48.937500, 48.093750, 0.031250).st("Andreas Horizons")
	d.sys("Demeter", -55.343750, 40.812500, 7.562500).st("*Demeter")
	d.sys("LP 180-17", -56.843750, 37.718750, 12.718750)
	d.sys("21 Draco", -60.031250, 45.656250, 8.093750).st("Williams Point")
	d.sys("GD 356", -50.937500, 44.156250, 7.312500)
	d.sys("Hagalaz", -50.593750, 45.156250, 11.093750)
	d.sys("CR Draco", -49.468750, 47.437500, 4.375000).st("Bruce Terminal")
	d.sys("CR Draconis", -48.312500, 46.156250, 4.468750).st("*CR Draconis")
	d.sys("Perkwunos", -40.500000, 46.750000, 6.750000)
	d.sys("LP 176-55", -37.281250, 50.968750, 10.093750)
	d.sys("Vaccimici", -33.218750, 37.343750, 12.750000).st("Gannt Enterprise")
	d.sys("Taran", -42.500000, 45.468750, -3.000000)
	d.sys("Surya", -38.468750, 39.250000, 5.406250).st("Burbank Camp")
	d.sys("LHS 5287", -36.406250, 48.187500, -0.781250).st("Mcarthur's Reach")
	d.sys("Chi Herculis", -30.750000, 39.718750, 12.781250).st("Gorbatko Reserve")
	d.sys("WREDGUIA LW-E D11-126", -54.000000, 56.093750, 4.468750)
	d.sys("LHS 2936", -31.687500, 55.562500, 0.500000).st("Mcmullen Installation")
	d.sys("LHS 2921", -32.562500, 64.625000, 3.593750)
	d.sys("Rahu", -43.781250, 62.437500, -0.250000).st("Weierstrass Refinery")
	d.sys("GD 319", -19.375000, 43.625000, -12.750000).st("*GD 319")
	d.sys("Aulis", -16.468750, 44.187500, -11.437500).st("Dezhurov Gateway")
	d.sys("Ross 690", -28.406250, 47.812500, -21.968750)
	d.sys("LHS 2819", -30.500000, 38.562500, -13.437500).st("Tasaki Freeport")
	d.sys("LB 2449", -26.062500, 51.625000, -17.406250)
	d.sys("BD+55 1519", -16.937500, 44.718750, -16.593750).st("*BD+55 1519")
	d.sys("Ithaca", -8.093750, 44.937500, -9.281250).st("Hume Depot")
	d.sys("47 Ursae Majoris", -1.437500, 41.187500, -20.093750).st("Rasmussen Relay")
	d.sys("Keian Gu", -6.000000, 36.281250, -19.875000).st("Ashton Enterprise")
	d.sys("LFT 898", -8.687500, 49.750000, -11.031250)
	d.sys("Mullag", 0.500000, 35.093750, -19.187500).st("Gardner Gateway")
	d.sys("CW Ursae Majoris", 2.656250, 36.781250, -14.906250).st("Pike Vision")
	d.sys("LP 320-359", 1.250000, 49.718750, -6.562500).st("Springer City")
	d.sys("WREGOE AC-D D12-135", -14.968750, 54.187500, -14.312500)
	d.sys("LHS 2651", -22.843750, 60.375000, -13.812500).st("Kippax Depot")
	d.sys("Loucetios", -21.437500, 58.062500, -10.156250)
	d.sys("WREGOE WV-E D11-120", -14.281250, 54.843750, -20.812500)
	d.sys("10 Canum Venaticorum", -9.375000, 55.437500, -7.000000).st("Trevithick Hub")
	d.sys("Parcae", -8.125000, 55.093750, -17.000000).st("Windt Plant")
	d.sys("Asellus Primus", -23.937500, 40.875000, -1.343750).st("Beagle 2 Landing", "Foster Research Lab")
	d.sys("i Bootis", -22.375000, 34.843750, 4.000000).st("Chango Dock", "Maher Stellar Research")
	d.sys("Eranin", -22.843750, 36.531250, -1.187500).st("Eranin 4 Survey", "Azeban Orbital", "Azeban City")
	d.sys("LP 98-132", -26.781250, 37.031250, -4.593750).st("Freeport", "Prospect Five")
	d.sys("Dahan", -19.750000, 41.781250, -3.187500).st("Dahan 3 Metalworks", "Dahan Gateway")
	d.sys("Morgor", -15.250000, 39.531250, -2.250000).st("Romanek's Folly")
	d.sys("LHS 2884", -22.000000, 48.406250, 1.781250).st("Abnett Platform")
	d.sys("Meliae", -17.312500, 49.531250, -1.687500).st("Dupuy De Lomes Pride")
	d.sys("Styx", -24.312500, 37.750000, 6.031250).st("Bixby Station", "Wingrove Platform")
	d.sys("Opala", -25.500000, 35.250000, 9.281250).st("Romanenko Estate")
	d.sys("LFT 992", -7.562500, 42.593750, 0.687500).st("Szulkin Mines")
	d.sys("Bolg", -7.906250, 34.718750, 2.125000).st("Moxon's Mojo", "Wang Terminal")
	d.sys("Ross 1008", -8.718750, 51.562500, 1.500000)
	d.sys("LP 322-836", -3.437500, 51.593750, 2.031250).st("Piccard Point")
	d.sys("Nanauatzin", -3.406250, 51.656250, 3.781250)
	d.sys("Avici", -5.750000, 42.531250, 8.281250)
	d.sys("LP 438-8", 2.000000, 40.250000, 10.312500)
	d.sys("Coelrind", 3.062500, 35.281250, 6.968750).st("Daimler Settlement")
	d.sys("Wolf 497", 0.125000, 41.468750, 11.968750).st("Kadenyuk Camp")
	d.sys("LHS 362", -3.218750, 38.031250, 8.406250)
	d.sys("Mufrid", -1.125000, 35.250000, 11.093750).st("*Mufrid")
	d.sys("Aganippe", -11.562500, 43.812500, 11.625000).st("Julian Market")
	d.sys("LHS 355", 1.093750, 46.156250, 9.718750).st("Glashow Settlement")
	d.sys("LHS 350", -1.468750, 44.593750, 5.750000).st("Wiley Port")
	d.sys("IZ Bootis", -23.625000, 61.718750, 7.937500)
	d.sys("Ori", -26.281250, 56.093750, 2.437500).st("Griffith Terminal")
	d.sys("Belobog", -13.343750, 53.781250, 12.562500).st("Clifford Point")
	d.sys("Ross 1019", -15.000000, 55.031250, 4.281250)
	d.sys("Ross 1007", -8.343750, 54.718750, 1.937500)
	d.sys("BF Canis Venatici", -8.250000, 62.187500, -3.062500).st("Lefschetz Installation")
	d.sys("LHS 2764a", -4.718750, 54.125000, 0.656250).st("Maclean Station")
	d.sys("Ross 640", -31.687500, 35.125000, 18.937500).st("Onnes Gateway")
	d.sys("LHS 3075", -34.875000, 51.875000, 12.906250)
	d.sys("V1090 Herculis", -44.937500, 36.937500, 13.468750).st("Low Landing")
	d.sys("Magec", -32.875000, 36.156250, 15.500000).st("Xiaoguan Hub")
	d.sys("Laima", -46.031250, 36.812500, 14.125000)
	d.sys("14 Herculis", -36.718750, 41.687500, 14.125000).st("Ejeta Station")
	d.sys("Ovid", -28.062500, 35.156250, 14.812500).st("Bradfield Orbital")
	d.sys("G 180-18", -28.968750, 40.687500, 19.031250).st("Murrays Port")
	d.sys("LHS 3080", -21.625000, 42.906250, 21.375000).st("Malyshev Depot")
	d.sys("LP 274-8", -29.718750, 45.000000, 20.281250)
	d.sys("LHS 391", -15.906250, 45.156250, 21.375000).st("Scheickart Depot")
	d.sys("Sigma Bootis", -14.468750, 47.437500, 14.406250).st("Dupuy De Lome Holdings")
	d.sys("LP 329-18", -26.625000, 39.875000, 23.281250)
	d.sys("Rho Coronae Borealis", -29.875000, 42.093750, 22.218750)
	d.sys("LHS 399", -13.562500, 36.156250, 25.781250)
	d.sys("Eta Coronae Borealis", -23.781250, 48.437500, 21.937500).st("*Eta Coronae Borealis")
	d.sys("LHS 3124", -26.281250, 43.437500, 24.187500)
	d.sys("Ross 130", -3.906250, 41.000000, 19.750000).st("Coleman Settlement")
	d.sys("LHS 6282", -11.468750, 39.781250, 22.781250).st("Stephens Terminal")
	d.sys("LHS 371", -9.625000, 49.437500, 17.625000).st("Pudwill Gorie Port")
	d.sys("LP 440-38", -4.656250, 43.781250, 20.687500).st("Reynolds Colony")
	d.sys("Ross 837", 1.812500, 39.937500, 15.531250)
	d.sys("LHS 2948", -21.687500, 60.437500, 15.093750).st("*LHS 2948")
	d.sys("Devi", -10.281250, 52.187500, 18.156250)

	SetDefaultData(&d.data)
}

func starid(s *System) (name, coords string) {
	var rv []rune
	uc := false
	for _, r := range s.Name {
		var rx rune
		switch {
		case ('a' <= r && r <= 'z') || ('0' <= r && r <= '9'):
			rx = r
		case 'A' <= r && r <= 'Z':
			rx = r + 'a' - 'A'
		case r == '\'':
			// no op
		default:
			uc = true
		}
		if rx != 0 {
			if uc {
				rv, uc = append(rv, '_'), false
			}
			rv = append(rv, rx)
		}
	}
	return string(rv), starcoordstr(s.P, 1)
}

func starcoordstr(p Vec3, prec float64) string {
	return scoord(p[0], prec, "e", "w") + scoord(p[1], prec, "u", "d") + scoord(p[2], prec, "n", "s")
}

func scoord(c, prec float64, pos, neg string) string {
	i := int((c * prec) + 0.5)
	var suffix string
	if i < 0 {
		i, suffix = -i, neg
	} else {
		suffix = pos
	}
	return strconv.Itoa(i) + suffix
}

func logf(f string, v ...interface{}) {
	fmt.Printf(f+"\n", v...)
}
