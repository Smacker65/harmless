package db

// Chart finds stars using spatial queries.
type Chart interface {
	// Visit calls fn for all stars
	Visit(fn func(*System))

	// Near calls fn for stars that are near c, likely within radius r.
	// Note that stars outside the sphere may be returned, use Sphere for
	// a strict test.
	Near(c Vec3, r float64, fn func(*System))

	// Sphere calls fn for stars that are in the sphere defined by c and r
	// along with the distance from c.
	Sphere(c Vec3, r float64, fn func(*System, float64))

	// Box calls fn for stars that are in the box defined by min and max
	Box(min, max Vec3, fn func(*System))
}

const MaxNodeLen = 8

type OctreeChart struct {
	root *node
}

func NewOctreeChart(sv []*System) *OctreeChart {
	return &OctreeChart{newOctreeRoot(sv)}
}

func (o *OctreeChart) Near(c Vec3, r float64, fn func(*System)) {
	min := Vec3{c[0] - r, c[1] - r, c[2] - r}
	max := Vec3{c[0] + r, c[1] + r, c[2] + r}
	o.root.visit(min, max, fn)
}

func (o *OctreeChart) Sphere(c Vec3, r float64, fn func(*System, float64)) {
	min := Vec3{c[0] - r, c[1] - r, c[2] - r}
	max := Vec3{c[0] + r, c[1] + r, c[2] + r}
	o.root.visit(min, max, func(s *System) {
		if dist := s.P.Sub(c).Abs(); dist <= r {
			fn(s, dist)
		}
	})
}

func (o *OctreeChart) Box(min, max Vec3, fn func(*System)) {
	o.root.visit(min, max, func(s *System) {
		v := s.P
		if min[0] <= v[0] && v[0] <= max[0] &&
			min[1] <= v[1] && v[1] <= max[1] &&
			min[2] <= v[2] && v[2] <= max[2] {
			fn(s)
		}
	})
}

func (o *OctreeChart) Visit(fn func(*System)) {
	o.root.visitall(fn)
}

type node struct {
	min, max Vec3
	children []*node   // 0 to 8 subtrees (empty ones omitted)
	stars    []*System // leaf only
}

func newOctreeRoot(sv []*System) *node {
	var min, max Vec3
	if len(sv) != 0 {
		v := sv[0].P
		min, max = v, v
	}
	for _, s := range sv {
		v := s.P
		for i := 0; i < 3; i++ {
			if v[i] < min[i] {
				min[i] = v[i]
			}
			if max[i] < v[i] {
				max[i] = v[i]
			}
		}
	}
	var d float64
	for k := 0; k < 3; k++ {
		dk := max[k] - min[k]
		if d < dk {
			d = dk
		}
	}
	d /= 2
	center := min.Add(max).Mul(0.5)

	root := &node{
		min:   Vec3{center[0] - d, center[1] - d, center[2] - d},
		max:   Vec3{center[0] + d, center[1] + d, center[2] + d},
		stars: sv,
	}
	root.split()
	return root
}

// visit calls fn for stars that may be within the box
// defined by min and max
func (n *node) visit(min, max Vec3, fn func(*System)) {
	if max[0] < n.min[0] || max[1] < n.min[1] || max[2] < n.min[2] ||
		n.max[0] < min[0] || n.max[1] < min[1] || n.max[2] < min[2] {
		return
	}
	for _, cn := range n.children {
		cn.visit(min, max, fn)
	}
	for _, s := range n.stars {
		fn(s)
	}
}

func (n *node) visitall(fn func(*System)) {
	for _, cn := range n.children {
		cn.visitall(fn)
	}
	for _, s := range n.stars {
		fn(s)
	}
}

func (n *node) split() {
	if len(n.stars) <= MaxNodeLen {
		return
	}
	subs := make([][]*System, 8)
	center := n.min.Add(n.max).Mul(0.5)
	for _, s := range n.stars {
		v := s.P
		idx := 0
		for k := 0; k < 3; k++ {
			if center[k] <= v[k] {
				idx |= 1 << uint(k)
			}
		}
		subs[idx] = append(subs[idx], s)
	}
	n.stars = nil
	for i, sv := range subs {
		if sv != nil {
			var cmin, cmax Vec3
			for k := 0; k < 3; k++ {
				if (i & (1 << uint(k))) == 0 {
					cmin[k], cmax[k] = n.min[k], center[k]
				} else {
					cmin[k], cmax[k] = center[k], n.max[k]
				}
			}
			cn := &node{
				min:   cmin,
				max:   cmax,
				stars: sv,
			}
			cn.split()
			n.children = append(n.children, cn)
		}
	}
}

func octant(center, pos Vec3) int {
	i := 0
	for k := 0; k < 3; k++ {
		if center[k] <= pos[k] {
			i |= 1 << uint(k)
		}
	}
	return i
}
