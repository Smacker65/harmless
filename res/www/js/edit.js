(function( starc, $, undefined ) {

	function inputField(entry, fdecl, changeFn, optarg) {
		var opts = $.extend({
			"class": "",
			itype: "text",
			defvalue: "",
			parse: function(s) { return s; },
			fmt: function(v) { return v.toString(); }
		}, optarg);
		var value = entry[fdecl.field];
		if (value === null || value === undefined) {
			value = opts.defvalue;
		}
		var lbl = $("<label>", {
			"class": optarg["class"],
			"for": fdecl.field
		}).text(fdecl.label);
		if (fdecl.info) {
			lbl.append($("<span>", {"class":"info"}).append($("<i>", {
				"class": "fa fa-info-circle",
				"title": fdecl.info
			})));
		}
		var inp = $("<input>", {
			"class": optarg["class"],
			name: fdecl.field,
			type: opts.itype,
			value: opts.fmt(value),
			placeholder: fdecl.label
		}).blur(function(e) {
			entry[fdecl.field] = opts.parse($(e.currentTarget).val());
			if (changeFn) changeFn(entry, fdecl);
		});
		if (fdecl.options) {
			inp.autocomplete({ minLength: 0, source: fdecl.options });
		}
		return $("<div>").append(lbl, inp);
	}

	function fieldFactory(cls) {
		var t = {
			str: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls
				});
			},
			num: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls,
					itype: "number",
					parse: function(s) { return parseFloat(s); }
				});
			},
			bignum: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls,
					defvalue: 0,
					parse: function(s) {
						var xs = s.replace(/,/g, "");
						if (xs == "") {
							return 0;
						}
						var v = xs.split(/ +/);
						var num = "", mul = 0;
						if (v.length > 0) {
							num = v[0];
						}
						if (v.length == 1) {
							mul = 1;
						} else if (v.length == 2) {
							var x = v[1].toLowerCase();
							if (x == "million" || x == "m") {
								mul = 1e6;
							}
							if (x == "billion" || x == "b") {
								mul = 1e9;
							}
						}
						if (isNaN(num) || mul == 0) {
							starc.showError("invalid number", s);
							return 0;
						}
						return parseFloat(num) * mul;
					},
					fmt: function(v) {
						if (v < 1e6) {
							return v.toString();
						} else if (v < 1e9) {
							return (v/1e6).toString() + " Million";
						}
						return (v/1e9).toString() + " Billion";
					}
				});
			},
			vstr: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls,
					defvalue: [],
					parse: function(s) {
						var v0 = s.split(",");
						var v = [];
						for (var i = 0; i < v0.length; i++) {
							var e = v0[i].trim();
							if (e != "") {
								v.push(e);
							}
						}
						return v;
					},
					fmt: function(v) { return v.join(", "); }
				});
			},
			bool: function(entry, fdecl, change) {
				return $("<button>", {
					"class": "toggle" + (entry[fdecl.field] == true ? " checked" : ""),
				}).text(fdecl.label).on("click", function(evt) {
					$(evt.currentTarget).toggleClass("checked");
					entry[fdecl.field] = $(evt.currentTarget).hasClass("checked");
					if (change) change(entry, fdecl);
				});
			},
			xbool: function(entry, fdecl, change) {
				var stateClass = " indeterminate";
				if (entry[fdecl.field] == true) {
					stateClass = " checked";
				} else if (entry[fdecl.field] == false) {
					stateClass = "";
				}
				return $("<button>", {
					"class": "toggle" + stateClass,
				}).text(fdecl.label).on("click", function(evt) {
					var b = $(evt.currentTarget);
					if (b.hasClass("checked")) {
						entry[fdecl.field] = false;
						b.removeClass("checked");
						b.removeClass("indeterminate");
					} else if (b.hasClass("indeterminate")) {
						entry[fdecl.field] = true;
						b.addClass("checked");
						b.removeClass("indeterminate");
					} else {
						entry[fdecl.field] = null;
						b.removeClass("checked");
						b.addClass("indeterminate");
					}
					if (change) change(entry, fdecl);
				});
			}
		};
		return function(entry, fdecl, change) {
			return t[fdecl.type](entry, fdecl, change);
		};
	};

	(function() {
		$.ajax({
			url: starc.apiurl("edit", { s: starc.prefs.locid }),
			dataType: "json",
			success: function(resp) {
				if (resp.error) {
					starc.showError("get system data", resp.error);
				} else if (!resp.data) {
					starc.showError("get system data", "response empty");
				} else {
					systemForm(resp.data);
				}
			},
			error: function(xhr, textstatus, errthrown) {
				starc.ajaxError("get system data", xhr, textstatus, errthrown);
			}
		});
	})();

	var currentVersion = "Gamma";

	function systemForm(s) {
		var c = $("#content");
		// title
		c.append($("<h1>", {"class": "system"})
				.text(s.system));
		c.append($("<span>").text(starc.coordstr(s.coords)));

		// valid checkbox
		var cbvalid = $("<input>", {
			type: "checkbox",
			name: "valid",
		}).change(function(e) {
			var v = $(e.currentTarget).is(":checked");
			touch(s);
			s.valid = v ? "invalid" : currentVersion;
		});

		if (s.valid == currentVersion) {
			cbvalid.val(true);
		} else if (s.valid == "invalid") {
			cbvalid.val(false);
		} else {
			cbvalid.prop("indeterminate", true);
		}
		$("<div>").appendTo(c).append(
			cbvalid,
			$("<label>", {
				"for": "valid"
			})
			.text("This system does not exist and should be removed from the chart. Make sure this box is unchecked before changing anything else."));

		// commander name
		$("<div>", { "style": "padding-top:1em;" }).appendTo(c).append(
			$("<label>", {
				"class": "field",
				"for": "cmdr"
			}).append(
				$("<span>").text("Commander "),
				$("<i>", {
					"class": "fa fa-info-circle",
					"title": "optional"
				})
			),
			$("<input>", {
				"class": "",
				type: "text",
				name: "cmdr",
				value: starc.prefs.cmdr
			})
			.blur(function(e) {
				starc.prefs.cmdr = $(e.currentTarget).val();
				starc.saveprefs();
			})
		);

		// system details
		c.append($("<h2>", { "class": "system" }).text("System"));
		var frm = $("<form>", { "class": "starc-form" }).appendTo(c);
		if (s.detail === null || s.detail === undefined) {
			s.detail = {};
		}
		var sysfields = [
			{ type: "str",    field: "allegiance", label: "Allegiance" },
			{ type: "str",    field: "economy",    label: "Economy",
				info: "main system economy as shown in the galaxy map" },
			{ type: "str",    field: "government", label: "Government" },
			{ type: "str",    field: "security",   label: "Security"   },
			{ type: "bignum", field: "population", label: "Population" }
		];
		var ff = fieldFactory("systemdetail");
		for (var i = 0; i < sysfields.length; i++) {
			var f = sysfields[i];
			if (starc.defs.hasOwnProperty(f.field)) {
				f.options = starc.defs[f.field];
			}
			frm.append(ff(s.detail, f, function() { touch(s) }));
		}

		// stations
		c.append($("<h2>", { "class": "stations" }).text("Stations"));
		c.append($("<div>", {
			id: "stations"
		}).sortable());
		var vstn = s.stations;
		if (vstn) {
			for (var i = 0; i < vstn.length; i++) {
				addStation(vstn[i]);
			}
		}

		// buttons
		c.append($("<div>")
			.append($("<button>", { "class": "formcontrol" }).append(
				$("<i>", {
					"class": "fa fa-plus"
				}),
				" Add Station"
			).on("click", function(e) {
				addStation({});
			}))
		);

		c.append($("<div>")
			.append($("<button>", { "class": "formcontrol" }).append(
				$("<i>", {
					"class": "fa fa-check"
				}),
				" Save"
			).on("click", function(e) {
				var sx = $.extend({}, s);
				sx.stations = [];
				$(".station").each(function(i, stnbox) {
					var stn = $(stnbox).data("stn");
					if (stn) {
						sx.stations.push(stn);
					}
				});
				if (consistencyCheck(sx)) {
					postSystem(sx);
				}
			}))
			.append($("<button>", { "class": "formcontrol" }).append(
				$("<i>", {
					"class": "fa fa-refresh"
				}),
				" Reload"
			).on("click", function(e) {
				window.location.reload();
			}))
		);
	}

	var stnfields = [
		{ type: "str",  field: "station", label: "Name" },
		{ type: "num",  field: "distance", label: "Distance", info: "from main star with nav beacon (Ls)" },
		{ type: "str",  field: "type", label: "Type", options: starc.defs.station_type },
		{ type: "str",  field: "faction", label: "Faction" },
		{ type: "str",  field: "government", label: "Government", options: starc.defs.government },
		{ type: "str",  field: "allegiance", label: "Allegiance", options: starc.defs.allegiance },
		{ type: "vstr", field: "economy", label: "Economy", info: "comma separated, from system map", options: starc.defs.economy },
		{ type: "xbool", field: "shipyard", label: "Shipyard" },
		{ type: "xbool", field: "outfitting", label: "Outfitting" },
		{ type: "xbool", field: "commodities_market", label: "Commodities" },
		{ type: "xbool", field: "black_market", label: "Black market" }
	];
	function addStation(stn) {
		var stnbox = $("<div>", {
			"class": "station"
		}).appendTo("#stations").data("stn", stn);
		var frm = stnbox;

		stnbox.append($("<button>", { "class": "close" }).append(
			$("<i>", {
				"title": "delete station",
				"class": "fa fa-times"
			})).on("click", function(e) {
				stnbox.fadeOut("slow", "swing", function() { stnbox.remove(); });
			}));

		var ff = fieldFactory("stationfield");
		for (var i = 0; i < stnfields.length; i++) {
			frm.append(ff(stn, stnfields[i], function(e) { touch(e) }));
		}
	}

	function consistencyCheck(s) {
		if (!s.detail && s.stations) {
			starc.showError("consistency", "has stations but no allegiance/economy");
			return false;
		}
		if (s.stations) {
			var dupname = {};
			for (var i = 0; i < s.stations.length; i++) {
				var stn = s.stations[i];
				if (stn.station === undefined || stn.station == "") {
					starc.showError("consistency", "station must have a name");
					return false;
				}
				if (dupname[stn.station.toLowerCase()]) {
					starc.showError("consistency", "duplicate station name: " + stn.station);
					return false;
				}
				dupname[stn.station.toLowerCase()] = true;
			}
		}
		return true;
	}

	function touch(sysOrStn) {
		sysOrStn.valid = currentVersion;
		var d = new Date();
		sysOrStn.modified = d.toISOString();
	}

	function postSystem(sys) {
		console.log(JSON.stringify(sys));
		$.ajax({
			type: "POST",
			url: starc.apiurl("edit"),
			dataType: "json",
			data: JSON.stringify({
				cmdr: starc.prefs.cmdr,
				data: sys
			}),
			success: function(resp) {
				if (resp.error) {
					starc.showError("post system data", resp.error);
				} else if (!resp.success) {
					starc.showError("post system data", "response empty");
				} else {
					starc.reportSuccess("Data posted. Good work, commander! Now reload this page to see if everything is in order.");
				}
			},
			error: function(xhr, textstatus, errthrown) {
				starc.ajaxError("post system data", xhr, textstatus, errthrown);
			}
		});
	}

})(window.starc = window.starc || {}, jQuery);
