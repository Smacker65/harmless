(function( starc, $, undefined ) {

	var apibase = $("meta[property='harmless:apiurl']").attr('content');
	starc.prefix = $("meta[property='harmless:prefix']").attr('content');

	starc.node = function(t, attrs, xmlns) {
		var n;
		if (typeof t === 'string') {
			if (xmlns == undefined) {
				n = document.createElement(t);
			} else {
				n = document.createElementNS(xmlns, t);
			}
		} else {
			n = t.cloneNode(true);
			xmlns = xmlns || n.getAttribute("xmlns");
		}
		if (attrs !== undefined) {
			for (var prop in attrs) {
				if (attrs.hasOwnProperty(prop)) {
					n.setAttribute(prop, attrs[prop].toString());
				}
			}
		}
		n.text = function(t) {
			n.appendChild(document.createTextNode(t));
		}
		if (xmlns == "") {
			n.child = function(t, attrs) {
				var c = starc.node(t, attrs);
				n.appendChild(c);
				return c;
			}
		} else {
			n.child = function(t, attrs) {
				var c = starc.node(t, attrs, xmlns);
				n.appendChild(c);
				return c;
			}
		}
		return n;
	};

	starc.ajaxError = function(op, xhr, textstatus, errthrown) {
		console.log(op);
		console.log(xhr, textstatus, errthrown);
		starc.showError(op, textstatus + ": " + errthrown);
	};

	starc.showError = function(op, msg) {
		var div = $("div#errormessage");
		if (div.length == 0) {
			div = starc.dlgbox("error", {id:"errormessage"});
		}
		$("<div>").appendTo(div).append(
			$("<i>", { "class": "fa fa-warning" }),
			$("<span>").text(" "+ op + ":"),
			$("<span>").text(" "+ msg)
		);
	};

	starc.reportSuccess = function(msg) {
		var div = starc.dlgbox("success");
		$("<div>").appendTo(div).append(
			$("<i>", { "class": "fa fa-info-circle" }),
			$("<span>").text(" "+ msg)
		);
	};

	starc.dlgbox = function(maincls, params) {
		var box = $("<div>", { "class": "dialogbox "+maincls });
		box.append($("<button>", { "class": "close" }).append(
			$("<i>", {
				"title": "dismiss",
				"class": "fa fa-times"
			})).on("click", function(e) {
				box.remove();
			}));
		box.appendTo(document.body);
		return $("<div>", $.extend({"class":"messages"}, params)).appendTo(box);
	}

	starc.defs = {
		station_type: [
			"Civilian Outpost",
			"Commercial Outpost",
			"Industrial Outpost",
			"Scientific Outpost",
			"Unknown Outpost",
			"Unsanctioned Outpost",
			"Coriolis Starport",
			"Ocellus Starport",
			"Orbis Starport"
		],
		station_population: [
			"tiny",
			"small",
			"medium", //?
			"large",
			"very large",
			"huge"
		],
		economy: [
			"None",
			"Agriculture",
			"Extraction",
			"High Tech",
			"Industrial",
			"Refinery",
			"Service",
			"Tourism"
		],
		allegiance: [
			"None",
			"Alliance",
			"Empire",
			"Federation",
			"Independent"
		],
		government: [
			"None",
			"Anarchy",
			"Colony",
			"Communism",
			"Confederacy",
			"Cooperative",
			"Corporate",
			"Democracy",
			"Dictatorship",
		],
		security: [ "none", "low", "medium", "high" ]
	};

	starc.defaultPrefs = {
		viewid: "sol", // view center
		locid: "sol", // selected system
		waypts: [], // route waypoints

		cmdr: "Jameson",

		ship: {
			MaxJumpDist: 6.97,
			RoutingMode: "x",
			MaxJumpFuel: 0.8,
			FuelTank: 2.0,
			FuelPower: 2.0,
			FuelUnits: 64
		},

		view: {
			radius: 40,
			scale: 1.2,
			unreachableOpacity: 0.4,
			colors: [
				{Type:"dot", Key:"economy", Value:"Agriculture", Color:"#90EE90"},
				{Type:"dot", Key:"economy", Value:"Refinery",    Color:"#6495ED"},
				{Type:"dot", Key:"economy", Value:"High Tech",   Color:"#FFFF00"},
				{Type:"dot", Key:"economy", Value:"Extraction",  Color:"#FF6347"},
				{Type:"dot", Key:"economy", Value:"Industrial",  Color:"#999944"},

				{Type:"label", Key:"security", Value:"low", Color:"#FF4500"}
			]
		}
	};
	starc.prefs = $.extend({}, starc.defaultPrefs);

	try {
		var pref = window.localStorage.getItem("harmless.prefs");
		starc.prefs = $.extend({}, starc.defaultPrefs, JSON.parse(pref));
	}
	catch (err) {}

	starc.saveprefs = function() {
		window.localStorage.setItem("harmless.prefs", JSON.stringify(starc.prefs));
	}

	starc.hashparams = function () {
		var h = window.location.hash;
		if (h == "") return {};
		return starc.params(h.substr(1));
	}

	starc.params = function (fragment) {
		if (fragment == "") return {};
		var a = fragment.split("&");
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p=a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	};

	starc.apiurl = function(api, p) {
		var q = apibase + "/" + api;
		if (p !== undefined) {
			q += "?" + $.param(p);
		}
		return q;
	};

	starc.coordstr = function(sc) {
		return Math.round(sc[0]).toString() +
				" : " + Math.round(sc[1]).toString() +
				" : " + Math.round(sc[2]).toString();
	}

})(window.starc = window.starc || {}, jQuery);
