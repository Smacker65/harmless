package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"sync"
)

// ClusterCache caches cluster maps for each jump range.
//
// The cluster map assigns an integer cluster id to each system.
// Each system having the same cluster id can be reached from all other
// systems sharing the same cluster id, with jumps smaller than or equal
// to the jump range.
type ClusterCache struct {
	mtx  sync.Mutex
	data map[int]*clusterNode
}

func (c *ClusterCache) Map(jumprange float64) map[*db.System]int {
	jri := int(jumprange * 100) // 2 dp

	c.mtx.Lock()
	if c.data == nil {
		c.data = make(map[int]*clusterNode)
	}
	node := c.data[jri]
	if node == nil {
		node = new(clusterNode)
		c.data[jri] = node
	}
	c.mtx.Unlock()

	node.once.Do(func() {
		node.data = mkClusterMap(float64(jri) / 100)
	})
	return node.data
}

type clusterNode struct {
	once sync.Once
	data map[*db.System]int
}

func mkClusterMap(jumprange float64) map[*db.System]int {
	set := make(map[*db.System]bool)
	for _, sys := range db.Systems {
		set[sys] = true
	}

	imap := make(map[*db.System]int)
	cluster := 1
	work := make([]*db.System, 0, len(set))
	for len(set) != 0 {
		var sys *db.System
		for sys, _ = range set {
			break
		}
		delete(set, sys)
		work = append(work[:0], sys)
		for len(work) != 0 {
			n := len(work) - 1
			work, sys = work[:n], work[n]
			imap[sys] = cluster

			db.DefaultData.Sphere(sys.P, jumprange, func(cmp *db.System, dist float64) {
				if set[cmp] {
					delete(set, cmp)
					work = append(work, cmp)
				}
			})
		}
		cluster++
	}

	return imap
}
