package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type editHandler struct {
	w io.Writer
}

func NewEditHandler(w io.Writer) *editHandler {
	return &editHandler{w}
}

func (h *editHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("editapifunc", req).Enter()
	defer errh.Exit()

	var resp sysResp
	if req.Method == "POST" {
		var p sysPost
		if err := json.NewDecoder(req.Body).Decode(&p); err != nil {
			resp.Err = err.Error()
		} else {
			p.RemoteAddr = RemoteAddr(req)
			err = h.saveSystem(p)
			if err == nil {
				resp.Success = true
			} else {
				resp.Err = err.Error()
			}
		}
	} else {
		s := db.IdSystem(req.FormValue("s"))
		if s == nil {
			resp.Err = fmt.Sprintf("invalid system: s=%q", req.FormValue("s"))
		} else {
			resp.Data = s
		}
	}

	errh.Err(ServeJson(w, req, resp))
}

func (h *editHandler) saveSystem(post sysPost) error {
	sys := post.Data
	if sys == nil {
		return fmt.Errorf("empty system data received")
	}
	oldsys := db.IdSystem(sys.Id)
	if oldsys == nil {
		return fmt.Errorf("unknown system, can't store")
	}
	oldsys.Sort()
	sys.Sort()
	sys.Modified = oldsys.Modified
	op, err := json.Marshal(oldsys)
	if err != nil {
		return err
	}
	np, err := json.Marshal(sys)
	if err != nil {
		return err
	}
	if bytes.Equal(op, np) {
		return fmt.Errorf("posted system has no changes")
	}

	sys.DbKey = oldsys.DbKey
	sys.Modified = time.Now().UTC()

	if err = json.NewEncoder(h.w).Encode(post); err != nil {
		return err
	}

	*oldsys = *sys // TODO race

	return nil
}

type sysResp struct {
	Data    *db.System `json:"data,omitempty"`
	Err     string     `json:"error,omitempty"`
	Success bool       `json:"success,omitempty"` // post response
}

type sysPost struct {
	Cmdr       string     `json:"cmdr"`
	Data       *db.System `json:"data"`
	RemoteAddr string     `json:"remoteAddr"`
}
