
Harmless
========

Utilities for Elite: Dangerous.


Installation
------------

The following steps are necessary to have the [route planner](http://tajti.co/elite/map/?s=i+Bootis)
running on your system.

* install go
* install gcc
* get the project and dependencies
* initialize the sqlite database
* start the server

Install [Go](http://golang.org) from the [Download page](https://golang.org/dl/), and set `GOPATH` environment
variable to point where you'd like to keep your sources.

An easy way to install gcc to be able to compile sqlite3 is to use [TDM-GCC](http://tdm-gcc.tdragon.net/download).

Assuming you have Git installed, you can get the project and its dependencies with:

`go get bitbucket.org/tajtiattila/harmless`

then cd into `src/bitbucket.org/tajtiattila/harmless` within your `GOPATH`.

There is a simple command line program for navigation, it is a good idea to try it to see if
everything works. Build it with:

`go get ./cmd/nav`

Then an example route from wyrd to G 239-25 can be planned with:

`./nav -ly 7.5 wyrd g239-25`

This will populate the sqlite database from the repository json data, should the sqlite database
be missing or out of date.

Finally, build the server with:

`go build`

You can start the server listening on http://localhost:6968/map with:

`harmless`


Trade Planner
-------------

Command line utilities for trade route calculation are also included. This was done for
Beta 1, in Beta 2 the trade data is not available anymore outside the game, so this part
of the project is abandoned for the time being.

A listener/subscriber to the Elite Market Data Network is provided.

There are two components, a listener and profit calculator.

Data is kept in .emdnclient subdirectory inside the home directory of the
current user.

The listener fetches data from firehose and feeds it to the local database.
It's best to keep it running in the background whenever possible. The profit
calculator will work only after enough data has been collected for all systems.

The profit calculator calculates trading routes according to the command line
parameters.

emdn2dn
-------

Emdn2dn listens to Firehose on the Elite Market Data Network, and writes the raw json into a
logfile and stores the information in a local sqlite database. There is an -import option
to replay the stored raw json file to recreate the database.

profit
------

Profit is the route calculator using the sqlite database created by the listener.

The option `-workset` sets the maximum number of routes to keep while processing.
More will need more memory, but better routes may be returned. The option `-timeout` limits
now long routing may run (use it with h, m, s, ms, u/µm of ns suffix), and `-search` limits
how many routes are collected before they are sorted by value (total profit/stops). Only
the best `-show` routes are displayer.

The starting point may be specified as an argument to `-from`. A desired target may be
specified with `-to`. Recurring paths may be generated with `-circle`. Only routes no longer than
`-maxlen` jumps will be generated.

Windows installation
--------------------

ZeroMQ on Go requires cgo. This is not a problem on POSIX systems, but it is a pain on Windows.

To install it, have a working GCC installation with basic MSYS system first.

Install MSYS Bash and Make, eg. using msysGit.

Install [TDM-GCC](from http://tdm-gcc.tdragon.net/). If using msysGit, do not add it to the path yet.

With having TDM-GCC-64 in D:/cgo/TDM-GCC-64, run the following commands insite `bash`:

	XCGO_ROOT=D:/cgo
	export PATH=$(cd $XCGO_ROOT && pwd)/TDM-GCC-64/bin:$PATH
	export C_INCLUDE_PATH=$(cd $XCGO_ROOT && pwd -W)/include
	export LIBRARY_PATH=$(cd $XCGO_ROOT && pwd -W)/lib

	mkdir -p $XCGO_ROOT/src

	cd $XCGO_ROOT/src
	curl http://download.zeromq.org/zeromq-4.0.4.tar.gz | tar xz
	cd zeromq-4.0.4
	./configure --prefix=$XCGO_ROOT
	make

If `make` fails, add

	#include <in6addr.h>

under

	#include <windows.h>

to the windows.hpp inside zeromq-4.0.4/src. Once make succeeds, run

	make install
	go get github.com/pebbe/zmq4

Scratchpad
----------

// example SQL query for SQLite db

```sql
	   SELECT i.name, l.system, m.buy, m.sell, m.stock,
	   			s.minb, s.avgb, s.maxb, s.mins, s.avgs, s.maxs
	   	FROM marketdata m, loc l, item i
	   		INNER JOIN (SELECT
	   				locationid, itemid,
	   				MIN(minbuy) AS minb,
	   				SUM(buy)/SUM(buydiv) AS avgb,
	   				MAX(maxbuy) AS maxb,
	   				MIN(minsell) AS mins,
	   				SUM(sell)/SUM(selldiv) AS avgs,
	   				MAX(maxsell) AS maxs
	   			FROM daily
	   			GROUP BY itemid, locationid) s
	   			ON m.locationid = s.locationid AND m.itemid = s.itemid
	   		WHERE l.id == m.locationid AND i.id == m.itemid
	   	ORDER BY i.name, m.buy;
```

Fuel Cost = Coefficient * (Distance * (Mass / Driverating))^Power
Distance = (Fuel Cost / Coefficient) ^ (1/Power) * (Driverating /Mass)
Mass = (Fuel Cost / Coefficient) ^ (1/Power) * (Driverating / Distance)

Coefficient = 0.023

           Emptymass Fueltank Reservoir Maxfuel/j  Driverating Fuelpower  MaxCargo
Anaconda      1984       64      1.0       48.0         2860       2.9         228
CobraMkIII     113        8      0.5        2.4          132       2.15         36
Eagle           52        2      0.25       0.8           59       2             4
Hauler          39        2      0.25       0.8           59       2            16
Sidewinder      42        2      0.25       0.8           59       2             4
Type-6         113        8      0.5        4.7          342       2.3         100
Type-9        1274       64      0.75      27.6         1800       2.75        440
Viper           25        5      0.5        0.8           59       2.0           8



Acknowledgements
----------------

Andreas Bogk for [EMDN](https://forums.frontier.co.uk/showthread.php?t=23585)
Oliver "kfsone" Smith for [Tradedangerous](https://forums.frontier.co.uk/showthread.php?t=34986)

FB Tuz for Ship range and usage data http://forums.frontier.co.uk/showthread.php?p=652351#post652351
https://docs.google.com/spreadsheets/d/1babZrFgJAm4-Ia4Gfhfh_QnuLrptq8C5Ax15RODUj7A/

This program was created using assets and imagery from Elite: Dangerous, with the permission
of Frontier Developments plc, for non-commercial purposes. It is not endorsed by nor reflects
the views or opinions of Frontier Developments and no employee of Frontier Developments was involved
in the making of it.


