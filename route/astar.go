package route

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"container/heap"
)

type AstarMap struct {
	jr Jumper
	ch db.Chart
	m  map[*db.System]map[*db.System]Route
}

func NewAstarMap(ch db.Chart, jr Jumper) *AstarMap {
	return &AstarMap{
		jr: jr,
		ch: ch,
		m:  make(map[*db.System]map[*db.System]Route),
	}
}

func (m *AstarMap) Route(a, b *db.System) Route {
	if _, ok := m.m[a]; !ok {
		m.m[a] = make(map[*db.System]Route)
	}
	if r, ok := m.m[a][b]; ok {
		return r
	}
	r := AstarRoute(m.ch, m.jr, a, b)
	m.m[a][b] = r
	return r
}

const astardbg = true

func astar(ch db.Chart, jr Jumper, a, b *db.System, fn func(*rlink) bool) {
	ft, fr := jr.FullTank(), jr.FullRange()

	h := makerlinkheap(&rlink{
		star: a,
		stat: Stat{
			FuelLeft: ft,
		},
		reach: ft,
	})

	var buf rlinkbuf

	seen := make(map[*db.System]*rvisit)

	for h.Len() != 0 {
		l := h.pop()
		ch.Sphere(l.star.P, fr, func(s *db.System, leg float64) {
			if leg <= fr && !l.visited(s) {
				if jcost := jr.JumpCost(l.stat.FuelLeft, leg); jcost != JumpInvalid {
					n := buf.newrlink(ft, leg, l, jcost, s)
					if n != nil {
						n.dist = s.P.Sub(a.P).Abssq()
						n.cost = jr.Evaluate(ft, &n.stat)
						rv := seen[s]
						if rv == nil {
							rv = new(rvisit)
							seen[s] = rv
						}
						if fn(n) && rv.add(n) {
							h.push(n)
						} else {
							buf.recycle(n)
						}
					}
				}
			}
		})
	}
}

func AstarRoute(ch db.Chart, jr Jumper, a, b *db.System) Route {
	if a == b {
		return &rlink{star: a}
	}

	var best *rlink
	mem := new(rlink)

	astar(ch, jr, a, b, func(n *rlink) bool {
		if best == nil || n.cost < best.cost {
			if n.star == b {
				*mem = *n
				best = mem
			} else {
				return true
			}
		}
		return false
	})

	if best != nil {
		return best.cleanup()
	}
	return nil
}

type rvisit struct {
	bestcost  *rlink
	bestreach *rlink
	rest      []*rlink
}

func (v *rvisit) add(l *rlink) (added bool) {
	if v.bestcost == nil {
		v.bestcost, v.bestreach = l, l
		return true
	}
	one := v.bestcost == v.bestreach
	if l.cost < v.bestcost.cost {
		if !one && v.bestcost.reach > l.reach {
			v.rest = append(v.rest, v.bestcost)
		}
		v.bestcost = l
		added = true
	}
	if l.reach > v.bestcost.reach {
		if v.bestreach != v.bestcost && v.bestreach.cost < l.cost {
			v.rest = append(v.rest, v.bestcost)
		}
		v.bestreach = l
		added = true
	}
	if one {
		return
	}
	for i, r := range v.rest {
		if r.reach >= l.reach && r.cost <= l.cost {
			return false
		}
		if l.reach >= r.reach && l.cost <= r.cost {
			v.rest[i] = l
			return true
		}
	}
	v.rest = append(v.rest, l)
	return true
}

type rlink struct {
	nref  int
	from  *rlink
	jcost int
	star  *db.System

	dist  float64
	cost  int64
	reach int

	stat Stat
}

type rlinkbuf struct {
	free *rlink
}

func (b *rlinkbuf) newrlink(ft int, leg float64, from *rlink, jcost int, star *db.System) (l *rlink) {
	reach := from.reach - jcost
	if reach < 0 {
		return nil
	}
	if b.free != nil {
		l, b.free = b.free, b.free.from
		l.from = from
		l.jcost = jcost
		l.star = star
	} else {
		l = &rlink{
			from:  from,
			jcost: jcost,
			star:  star,
		}
	}
	from.nref++
	l.stat.NumJumps = from.stat.NumJumps + 1
	l.stat.TotalLen = from.stat.TotalLen + leg
	if star.CanRefuel() {
		l.reach = ft
	} else {
		l.reach = from.reach - jcost
	}
	l.stat.FuelLeft = from.stat.FuelLeft - jcost
	l.stat.FuelTaken = from.stat.FuelTaken
	l.stat.NumRefuels = from.stat.NumRefuels
	if l.stat.FuelLeft >= 0 {
		return l
	}
	l.stat.FuelTaken += l.reach - l.stat.FuelLeft
	l.stat.NumRefuels++
	l.stat.FuelLeft = reach
	return l
}

func (b *rlinkbuf) recycle(l *rlink) {
	l.from.nref--
	b.free, l.from = l, b.free
}

// cleanup sets final fuel left values
// so Visit can report valid refuel points
func (l *rlink) cleanup() *rlink {
	v := make([]*rlink, 0, l.stat.NumJumps+1)
	l.visit(func(l *rlink) {
		v = append(v, l)
	})
	fulltank := v[0].stat.FuelLeft
	tank, i, fi := fulltank, 1, -1
	for i < len(v) {
		ntank := tank - v[i].jcost
		v[i].stat.FuelLeft = ntank
		if ntank >= 0 {
			if v[i].star.CanRefuel() && i >= fi {
				fi = i
			}
			tank, i = ntank, i+1
		} else if fi > 0 {
			tank = fulltank
			v[fi].stat.FuelLeft = tank
			i, fi = fi+1, -1
		}
	}
	return l
}

func (l *rlink) Visit(fn VisitRouteFunc) {
	var (
		tank     int
		refueled bool
	)
	l.visit(func(l *rlink) {
		if l.from != nil {
			tank -= l.jcost
			fn(l.from.star, l.star, refueled)
			refueled = l.stat.FuelLeft > tank
		}
		tank = l.stat.FuelLeft
	})
}

func (l *rlink) Stat() Stat {
	return l.stat
}

func (l *rlink) String() string {
	v := make([]rlink, l.stat.NumJumps+1)
	i := 0
	l.visit(func(l *rlink) {
		v[i] = *l
		if i != 0 {
			v[i].from = &v[i-1]
		}
		i++
	})
	lc := v[i-1].cleanup()
	s := ""
	lc.Visit(func(a, b *db.System, refuel bool) {
		if s == "" {
			s = a.Name
		}
		if refuel {
			s += "@"
		}
		s += "→" + b.Name
	})
	return s
}

func (l *rlink) InternString() string { return "«A* route»" }

func (l *rlink) visited(s *db.System) bool {
	for {
		if l == nil {
			return false
		}
		if l.star == s {
			return true
		}
		l = l.from
	}
	return false // not reached
}

func (l *rlink) visit(fn func(*rlink)) {
	if l.from != nil {
		l.from.visit(fn)
	}
	fn(l)
}

func (l *rlink) visitjcost(fn func(a, b *db.System, jcost int)) {
	if l.from != nil {
		l.from.visitjcost(fn)
		fn(l.from.star, l.star, l.jcost)
	}
}

type rlinkheap struct {
	v []*rlink
}

func makerlinkheap(start *rlink) rlinkheap {
	return rlinkheap{[]*rlink{start}}
}

func (h *rlinkheap) Len() int             { return len(h.v) }
func (h *rlinkheap) Swap(i, j int)        { h.v[i], h.v[j] = h.v[j], h.v[i] }
func (h *rlinkheap) Push(x interface{})   { h.v = append(h.v, x.(*rlink)) }
func (h *rlinkheap) Pop() (r interface{}) { r, h.v = h.v[len(h.v)-1], h.v[:len(h.v)-1]; return }

func (h *rlinkheap) Less(i, j int) bool {
	li, lj := h.v[i], h.v[j]
	cmp := li.dist - lj.dist
	switch {
	case cmp < 0:
		return true
	case cmp == 0:
		return li.cost < lj.cost
	}
	return false
}

func (h *rlinkheap) push(r *rlink) { heap.Push(h, r) }
func (h *rlinkheap) pop() *rlink   { return heap.Pop(h).(*rlink) }
