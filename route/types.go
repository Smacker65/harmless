package route

import (
	. "bitbucket.org/tajtiattila/harmless/db"
)

const (
	μ           = 0.023
	JumpInvalid = 1e9
)

// Jump cost calculation:
//    f: fuel cost
//    d: distance
//    m: Mass
//    r: DriveRating
//    e: DriveExp
//    μ: 0.023
//
// f = μ·(d*m/r)^e
// d = (f/μ)^(1/e)*(r/m)
// m = (f/μ)^(1/e)*(r/d)

// Router finds routes between stars. The returned
// route may be nil, if the destination is unreachable.
type Router interface {
	Route(a, b *System) Route
}

// A Jumper can tell if a hiperspace jumps is possible, and how much fuel is left thereafter.
type Jumper interface {
	Evaluator

	// JumpCost calculates the fuel cost of a jump.
	JumpCost(fuelu int, jumpdist float64) (fuelcost int)

	// Return the full tank value for this ship.
	FullTank() int

	// FullRange returns the maximal jumpdist that can potentially performed.
	FullRange() float64
}

// VisitRouteFunc is called from Route.Visit for each jump in
// the route. Fulltank is true if a refuel is necessary at a.
type VisitRouteFunc func(a, b *System, fulltank bool)

type Route interface {
	// Visit calls fn for every jump in order in this route.
	Visit(fn VisitRouteFunc)

	// Stat returns the jump statistics with number of jumps and
	// number of refuels necessary on this route, along with fuel
	// remaining at the destination.
	Stat() Stat
}

type Stat struct {
	TotalLen   float64 // total distance in Ly
	NumJumps   int
	NumRefuels int
	FuelLeft   int // fuel left as calculated according to NumRefuels
	FuelTaken  int // fuel taken (via refueling) along this route so far
}

func (s *Stat) FuelUsed(fulltankunits int) int {
	tankuse := fulltankunits - s.FuelLeft
	return tankuse + s.FuelTaken
}

// Evaluator determines how much a route costs according to
// the optimization rules in effect.
type Evaluator interface {
	Evaluate(fulltankunits int, stat *Stat) int64
}

var (
	// OptRefuels optimizes routes for time. It tries to minimize number of refuel points,
	// number of jumps, amount of fuel left at the destination, and total length in this order.
	OptTime Evaluator = &optTime{}

	// OptFuelUse optimizes routes for minimal fuel use. It tries to minimize fuel taken,
	// then number of refuel points, number of jumps, amount of fuel left at the destination,
	// and total length in this order.
	OptFuelUse Evaluator = &optFuelUse{}
)

type optTime struct{}

func (*optTime) Evaluate(fulltankunits int, s *Stat) int64 {
	return 1000*int64(s.NumRefuels) + 100*int64(s.NumJumps) + int64(s.FuelTaken-s.FuelLeft)
}

type optFuelUse struct{}

func (*optFuelUse) Evaluate(fulltankunits int, s *Stat) int64 {
	return 1000*int64(s.FuelUsed(fulltankunits)) + int64(10*s.NumRefuels+s.NumJumps)
}
