package route

import (
	. "bitbucket.org/tajtiattila/harmless/db"
)

// IncOctreeChart is an octree that supports incremental insert
type IncOctreeChart struct {
	root *xnode
}

type xnode struct {
	min, max Vec3
	children []*xnode  // nil or 8 subtrees
	stars    []*System // leaf only
}

func NewIncOctreeChart() *IncOctreeChart {
	return &IncOctreeChart{root: new(xnode)}
}

func (o *IncOctreeChart) Sphere(c Vec3, r float64, fn func(*System, float64)) {
	min := Vec3{c[0] - r, c[1] - r, c[2] - r}
	max := Vec3{c[0] + r, c[1] + r, c[2] + r}
	o.root.visit(min, max, func(s *System) {
		if dist := s.P.Sub(c).Abs(); dist <= r {
			fn(s, dist)
		}
	})
}

func (o *IncOctreeChart) Box(min, max Vec3, fn func(*System)) {
	o.root.visit(min, max, func(s *System) {
		v := s.P
		if min[0] <= v[0] && v[0] <= max[0] &&
			min[1] <= v[1] && v[1] <= max[1] &&
			min[2] <= v[2] && v[2] <= max[2] {
			fn(s)
		}
	})
}

func (o *IncOctreeChart) Visit(fn func(*System)) {
	o.root.visitall(fn)
}

func (o *IncOctreeChart) Insert(s *System) {
	r := o.root
	if r.children == nil {
		// root has no children, update min/max as needed
		if r.stars == nil {
			for k := 0; k < 3; k++ {
				r.min[k] = s.P[k]
				r.max[k] = s.P[k]
			}
		} else {
			for k := 0; k < 3; k++ {
				if s.P[k] < r.min[k] {
					r.min[k] = s.P[k]
				}
				if r.max[k] < s.P[k] {
					r.max[k] = s.P[k]
				}
			}
		}
		if len(r.stars) == MaxNodeLen {
			// root is at capacity, make it a proper cube
			// before it gets split up
			var d float64
			for k := 0; k < 3; k++ {
				dk := r.max[k] - r.min[k]
				if d < dk {
					d = dk
				}
			}
			d /= 2
			c := r.min.Add(r.max).Mul(0.5)
			r.min = Vec3{c[0] - d, c[1] - d, c[2] - d}
			r.max = Vec3{c[0] + d, c[1] + d, c[2] + d}
		}
	}
	for {
		// check if the new star is within the bounds of root
		cidx, grow := 0, false
		for k := 0; k < 3; k++ {
			switch {
			case s.P[k] < r.min[k]:
				cidx |= 1 << uint(k)
				grow = true
			case r.max[k] < s.P[k]:
				grow = true
			}
		}
		if !grow {
			break
		}
		// create new root, expanding the tree in
		// the direction of the new star
		min, max := r.min, r.max
		for k := 0; k < 3; k++ {
			dk := r.max[k] - r.min[k]
			if (cidx & 1 << uint(k)) == 0 {
				min[k] = r.min[k]
				max[k] = r.max[k] + dk
			} else {
				min[k] = r.min[k] - dk
				max[k] = r.max[k]
			}
		}
		r = &xnode{min: min, max: max, children: make([]*xnode, 8)}
		r.children[cidx] = o.root
		r.addmissingchildren()
		o.root = r
	}
	r.insert(s)
}

func (n *xnode) insert(sn *System) {
	if len(n.stars) == MaxNodeLen {
		n.split()
	}
	if n.children != nil {
		n.children[octant(n.min.Add(n.max).Mul(0.5), sn.P)].insert(sn)
	} else {
		n.stars = append(n.stars, sn)
	}
}

// visit calls fn for stars that may be within the box
// defined by min and max
func (n *xnode) visit(min, max Vec3, fn func(*System)) {
	if (max[0] < n.min[0] && max[1] < n.min[1] && max[2] < n.min[2]) ||
		(n.max[0] < min[0] && n.max[1] < min[1] && n.max[2] < min[2]) {
		return
	}
	for _, cn := range n.children {
		cn.visit(min, max, fn)
	}
	for _, s := range n.stars {
		fn(s)
	}
}

func (n *xnode) visitall(fn func(*System)) {
	for _, cn := range n.children {
		cn.visitall(fn)
	}
	for _, s := range n.stars {
		fn(s)
	}
}

func (n *xnode) split() {
	center := n.min.Add(n.max).Mul(0.5)
	n.children = make([]*xnode, 8)
	n.addmissingchildren()
	for _, s := range n.stars {
		n.children[octant(center, s.P)].insert(s)
	}
	n.stars = nil
}

func (n *xnode) addmissingchildren() {
	centr := n.min.Add(n.max).Mul(0.5)
	for i := 0; i < 8; i++ {
		if n.children[i] == nil {
			cn := new(xnode)
			for k := 0; k < 3; k++ {
				if (i & (1 << uint(k))) == 0 {
					cn.min[k] = n.min[k]
					cn.max[k] = centr[k]
				} else {
					cn.min[k] = centr[k]
					cn.max[k] = n.max[k]
				}
			}
			n.children[i] = cn
		}
	}
}

func octant(center, pos Vec3) int {
	i := 0
	for k := 0; k < 3; k++ {
		if center[k] <= pos[k] {
			i |= 1 << uint(k)
		}
	}
	return i
}
