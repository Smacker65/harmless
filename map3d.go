package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"encoding/json"
	"fmt"
	"net/http"
)

type tsys struct {
	Id   string `json:"id"`
	Name string `json:"sys"`

	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`

	Signif     float64 `json:"significance"`
	HasStation bool    `json:"station,omitempty"`

	Pop   int64  `json:"pop,omitempty"`
	Econ  string `json:"econ,omitempty"`
	Govt  string `json:"govt,omitempty"`
	Alleg string `json:"alleg,omitempty"`
}

func mapdatafunc(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("mapdatafunc", req).Enter()
	defer errh.Exit()

	ww := NewJsonWriter(w, req)
	fmt.Fprint(ww, "[")
	defer Close(ww)
	enc := json.NewEncoder(ww)

	sep := false
	vsys := db.DefaultData.Systems
	for _, sys := range vsys {
		t := tsys{
			Name: sys.Name,
			Id:   sys.Id,
			X:    sys.P[0],
			Y:    sys.P[1],
			Z:    sys.P[2],

			Signif:     sys.Signif,
			HasStation: len(sys.Stations) != 0,
		}
		if sd := sys.SystemDetail; sd != nil {
			t.Pop = sd.Pop
			t.Econ = sd.Econ
			t.Govt = sd.Govt
			t.Alleg = sd.Alleg
		}
		if sep {
			fmt.Fprint(ww, ",")
		}
		errh.Err(enc.Encode(t))
		sep = true
	}
	fmt.Fprint(ww, "]")
}
