package planner

import (
	. "bitbucket.org/tajtiattila/harmless"
	"encoding/json"
	"time"
)

type Query struct {
	Ship     ShipSpec `json:"ship"`
	Loadout  float64  `json:"loadout"`
	Cash     int      `json:"cash"`
	Dock     float64  `json:"dock"`
	MinStock int      `json:"minstock"`
	From     StarSpec `json:"from"`
	To       StarSpec `json:"to"`
	Circle   bool     `json:"circle"`
	Age      Duration `json:"age"`
	MaxLen   int      `json:"maxlen"`
}

type Config struct {
	Show           int      `json:"show"`
	SearchLimit    int      `json:"searchlimit"`
	Timeout        Duration `json:"timeout"`
	WorkingSetSize int      `json:"workingsetsize"`
}

type Duration struct {
	V time.Duration
}

func (d Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(d.V.String())
}

func (d *Duration) UnmarshalJSON(p []byte) (err error) {
	var s string
	if err = json.Unmarshal(p, &s); err == nil {
		d.V, err = time.ParseDuration(s)
	}
	return
}

type ShipSpec struct {
	P *Ship
}

func (c *ShipSpec) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.P.Name)
}

func (c *ShipSpec) UnmarshalJSON(p []byte) (err error) {
	var s string
	if err = json.Unmarshal(p, &s); err == nil {
		c.P, err = FindShip(s)
	}
	return
}

func (c *ShipSpec) String() string {
	return c.P.Name
}

func (c *ShipSpec) Set(s string) (err error) {
	c.P, err = FindShip(s)
	return err
}

type StarSpec struct {
	P *System
}

func (c *StarSpec) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.P.Name)
}

func (c *StarSpec) UnmarshalJSON(p []byte) (err error) {
	var s string
	if err = json.Unmarshal(p, &s); err == nil {
		c.P, err = FindSystem(s)
	}
	return
}

func (c *StarSpec) String() string {
	if c.P != nil {
		return c.P.Name
	}
	return ""
}

func (c *StarSpec) Set(s string) (err error) {
	if s != "" {
		c.P, err = FindSystem(s)
	} else {
		c.P = nil
	}
	return err
}
