package planner

import (
	. "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/market"
	"bitbucket.org/tajtiattila/harmless/route"
)

type Trader struct {
	limits Config
	ship   *Ship
	jr     route.Jumper
	jm     route.Router
}

func NewTrader(c Config) *Trader {
	return &Trader{limits: c}
}

func (t *Trader) Query(mds market.DataSource, q *Query, c *Config) *Result {
	sc := t.limits
	if c != nil {
		if c.Show < sc.Show {
			sc.Show = c.Show
		}
		if c.SearchLimit < sc.SearchLimit {
			sc.SearchLimit = c.SearchLimit
		}
		if c.Timeout.V < sc.Timeout.V {
			sc.Timeout = c.Timeout
		}
		if c.WorkingSetSize < sc.WorkingSetSize {
			sc.WorkingSetSize = c.WorkingSetSize
		}
	}
	if t.ship != q.Ship.P {
		t.ship = q.Ship.P
		t.jr = route.NewShipJumper(t.ship)
		t.jm = route.NewAstarMap(t.jr)
	}
	tr := &tradeRun{q, sc, t.jr, t.jm, 0}
	return tr.run(mds)
}
