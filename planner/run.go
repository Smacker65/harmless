package planner

import (
	. "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/market"
	"bitbucket.org/tajtiattila/harmless/route"
	"sort"
	"time"
)

type tradeRun struct {
	q   *Query
	cfg Config

	jr      route.Jumper
	jm      route.Router
	bestret float64
}

func (tr *tradeRun) run(mds market.DataSource) *Result {
	var v []trentry
	tstart := time.Now()
	tstop := tstart.Add(tr.cfg.Timeout.V)
	q := tr.q
	from, dest := q.From.P, q.To.P
	err := FindRoutes(
		mds,
		tr.cfg.WorkingSetSize,
		tr.HopValueFunc(),
		func(r *TradeRoute) (extend bool, keepgoing bool) {
			keepgoing = time.Now().Before(tstop)
			if q.Circle {
				if from == nil {
					if len(r.Hops) == 1 {
						hop, ok := tr.besthop(r.Hops[0])
						extend = ok && tr.bestret*2 < hop.ret()*3
					} else {
						extend = true
					}
				} else {
					extend = from == r.Head()
				}
				if r.Head() != r.Tail() {
					return
				}
				found := from == nil
				for i := 0; !found && i < r.Len(); i++ {
					found = r.System(i) == from
				}
				if !found {
					return
				}
			} else {
				if from != nil && r.Head() != from {
					return
				}
				if dest != nil && r.Tail() != dest {
					return
				}
				extend = true
			}
			if q.MaxLen <= len(r.Hops) {
				extend = false
			}
			if q.MaxLen < len(r.Hops) {
				return
			}
			profit, effort := tr.routeStats(r, q.Dock)
			v = append(v, trentry{profit, effort, r})
			return
		})

	sort.Sort(sort.Reverse(trentrysort(v)))

	nfound := len(v)
	if tr.cfg.Show < len(v) {
		v = v[:tr.cfg.Show]
	}

	return &Result{
		Ship:     q.Ship.P,
		Cash:     q.Cash,
		Err:      err,
		Time:     time.Now(),
		Duration: time.Now().Sub(tstart),
		NumFound: nfound,
		Routes:   tr.result(v),
	}
}

func (tr *tradeRun) result(v []trentry) []RouteResult {
	rv := make([]RouteResult, len(v))
	for i, e := range v {
		var (
			total market.Price
			hr    []HopResult
		)
		for _, h := range e.r.Hops {
			best, _ := tr.besthop(h)
			total += market.Price(best.qty) * best.profit
			hr = append(hr, tr.hopresult(h))
		}
		rv[i] = RouteResult{
			AvgProfit: int(float64(total) / float64(len(hr))),
			Profit:    int(total),
			Hops:      hr,
		}
	}
	return rv
}

func (tr *tradeRun) hopresult(h *TradeHop) HopResult {
	var vh []hopdata
	tr.find(h, func(h hopdata) {
		if h.profit > 0 {
			vh = append(vh, h)
		}
	})
	sort.Sort(sort.Reverse(&hopsort{vh, func(h hopdata) float64 {
		return h.ret()
	}}))
	r := tr.jm.Route(h.From, h.To)
	_, _, fuelremaining := r.Stats()
	hr := HopResult{
		From:          h.From,
		To:            h.To,
		FuelRemaining: int(100 * fuelremaining / tr.jr.FullTank()),
	}
	r.Visit(func(a, b *System, fulltank bool) {
		if a != h.From {
			// if jump is made with full tank, ship had been refueled
			hr.Route = append(hr.Route, RouteSystem{a, fulltank})
		}
	})
	for _, hd := range vh {
		hr.Items = append(hr.Items, TradeItem{
			T:           hd.t,
			Qty:         hd.qty,
			Profit:      int(hd.profit),
			TotalProfit: hd.qty * int(hd.profit),
		})
	}
	return hr
}

func (t *tradeRun) HopValueFunc() HopValueFunc {
	t.bestret = 0
	return func(h *TradeHop) float64 {
		best, ok := t.besthop(h)
		if ok {
			r := best.ret()
			if t.bestret < r {
				t.bestret = r
			}
			return r
		}
		return -1
	}
}

func (t *tradeRun) besthop(h *TradeHop) (best hopdata, ok bool) {
	var v float64
	t.find(h, func(h hopdata) {
		ret := h.ret()
		if v < ret {
			v, best = ret, h
			ok = true
		}
	})
	return
}

func (tr *tradeRun) routeStats(r *TradeRoute, dock float64) (profit market.Price, effort float64) {
	for _, h := range r.Hops {
		best, _ := tr.besthop(h)
		profit += market.Price(best.qty) * best.profit
		njumps, nrefuels, _ := tr.jm.Route(h.From, h.To).Stats()
		effort += float64(nrefuels+1) + float64(njumps)/dock
	}
	return
}

type trentry struct {
	profit market.Price
	effort float64
	r      *TradeRoute
}

func (e trentry) val() float64 {
	return float64(e.profit) / e.effort
}

type trentrysort []trentry

func (s trentrysort) Len() int           { return len(s) }
func (s trentrysort) Less(i, j int) bool { return s[i].val() < s[j].val() }
func (s trentrysort) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

type hopsort struct {
	v []hopdata
	f func(h hopdata) float64
}

func (s *hopsort) Len() int           { return len(s.v) }
func (s *hopsort) Less(i, j int) bool { return s.f(s.v[i]) < s.f(s.v[j]) }
func (s *hopsort) Swap(i, j int)      { s.v[i], s.v[j] = s.v[j], s.v[i] }

type hopdata struct {
	t      TradeHopTrade
	qty    int
	profit market.Price
	effort float64
}

func (h hopdata) ret() float64 {
	return float64(h.qty) * float64(h.profit) / h.effort
}

func (tr *tradeRun) find(h *TradeHop, fm func(h hopdata)) {
	if h.From == h.To {
		return
	}
	r := tr.jm.Route(h.From, h.To)
	if r == nil {
		return
	}
	njumps, nrefuels, _ := r.Stats()
	effort := float64(nrefuels+1) + float64(njumps)/tr.q.Dock
	for _, t := range h.Trades {
		if t.Buy.Qty < tr.q.MinStock {
			continue
		}
		if t.Buy.Price < t.Buy.Daily.Avg/3 {
			continue // invalid price?
		}
		qty := tr.q.Ship.P.Cap
		if t.Buy.Price != 0 {
			afford := tr.q.Cash / int(t.Buy.Price)
			if afford < qty {
				qty = afford
			}
		}
		if t.Buy.Qty < qty {
			qty = t.Buy.Qty // stock
		}
		profit := t.Sell.Price - t.Buy.Price
		fm(hopdata{t, qty, profit, effort})
	}
}
